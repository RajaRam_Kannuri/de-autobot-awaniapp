/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const { ReportAggregator, HtmlReporter } = require('@rpii/wdio-html-reporter')
var log4js = require('log4js')
var browserstack = require('browserstack-local')
require('babel-polyfill')
require('dotenv/config')
require('@babel/register')({
  presets: ['@babel/preset-env']
})

const environment = require('./environment')
const chrome = require(`./src/${environment.environment.testProject}/cfg/capabilities/chrome_bstack`)
const firefox = require(`./src/${environment.environment.testProject}/cfg/capabilities/firefox_bstack`)
const safari = require(`./src/${environment.environment.testProject}/cfg/capabilities/safari_bstack`)
const ie = require(`./src/${environment.environment.testProject}/cfg/capabilities/ie_bstack`)
const ios = require(`./src/${environment.environment.testProject}/cfg/capabilities/ios_bstack`)
const android = require(`./src/${environment.environment.testProject}/cfg/capabilities/android_bstack`)
let prodUrl = 'http://www.astroawani.com/'
let stgUrl = 'https://de-awani-web-portal-stg.eco.astro.com.my/'
let devUrl = 'https://de-awani-web-portal-dev.eco.astro.com.my/'

const debugTimeout = 24 * 60 * 60 * 1000

const stepsFolder = `./src/${environment.environment.testProject}/step-definitions`
const fs = require('fs-extra')
var steps = []
fs.readdirSync(stepsFolder).forEach(file => {
  step = stepsFolder + '/' + file
  steps.push(step)
})

const config = {
  user: environment.environment.bstackuser,
  key: environment.environment.bstackaccesskey,
  browserstack: {
    host: 'hub.browserstack.com',
    port: 80, // can have values 443/80
    path: '/wd/hub'
  },
  runner: 'local',
  deprecationWarnings: false,
  seleniumInstallArgs: { version: '3.4.0' },
  seleniumArgs: { version: '3.4.0' },
  specs: environment.environment.specs.split(','),
  debug: environment.environment.debug,
  maxInstances: environment.environment.maxInstances,
  sync: true,
  logLevel: environment.environment.debug ? 'verbose' : environment.environment.logLevel,
  coloredLogs: true,
  screenshotsPath: './errorShots/',
  waitforTimeout: environment.environment.waitforTimeout,
  connectionRetryTimeout: environment.environment.connectionRetryTimeout,
  connectionRetryCount: 3,
  seleniumLogs: './reports/selenium',
  framework: environment.environment.framework,
  reporters: ['allure', [HtmlReporter, {
    debug: true,
    outputDir: './reports/html-reports/',
    fileName: 'report.html',
    showInBrowser: true,
    // to initialize the logger
    LOG: log4js.getLogger('info')
  }]], // 'dot', 'junit', 'json',
  startTimeout: environment.environment.waitforTimeout,
  eventWait: environment.environment.waitforTimeout,
  services: [],
  capabilities: [],
  reporterOptions: {
    allure: {
      outputDir: './allure-results',
      disableWebdriverStepsReporting: false,
      disableWebdriverScreenshotsReporting: false,
      useCucumberStepReporter: true,
      outputFileFormat: function (opts) {
        let platform
        if (opts.capabilities.includes('ios')) {
          platform = 'ios'
        } else if (opts.capabilities.includes('android')) {
          platform = 'android'
        } else {
          platform = 'web'
        }
        return `${platform}_astro_${opts.cid}.xml`
      }
    }
  },
  mochaOpts: {
    ui: 'bdd',
    timeout: environment.environment.debug ? debugTimeout : 180000,
    grep: environment.environment.tags ? environment.environment.tags.split('|') : []
  },
  cucumberOpts: {
    require: steps,
    backtrace: false,
    compiler: [],
    dryRun: false,
    failFast: false,
    format: ['pretty'],
    colors: true,
    snippets: true,
    source: true,
    profile: [],
    strict: false,
    tags: environment.environment.tags ? environment.environment.tags.split('|') : [],
    timeout: environment.environment.debug ? debugTimeout : 180000,
    requireModule: ['@babel/register'],
    ignoreUndefinedDefinitions: false
  },
  onPrepare: function (config, capabilities) {
    const reportAggregator = new ReportAggregator({
      outputDir: './reports/html-reports/',
      filename: 'master-report.html',
      reportTitle: 'Master Report',
      browserName: environment.environment.targetDevice
      // to use the template override option, can point to your own file in the test project:
      // templateFilename: path.resolve(__dirname, '../template/wdio-html-reporter-alt-template.hbs')
    })
    reportAggregator.clean()

    global.reportAggregator = reportAggregator

    // to start browser stack locally before the test execution starts
    if (environment.environment.testEnvironment === 'browserstack') {
      console.log('Connecting local')
      return new Promise(function (resolve, reject) {
        exports.bs_local = new browserstack.Local()
        exports.bs_local.start({ key: config.key, force: true }, function (error) {
          if (error) return reject(error)

          console.log('Connected. Now testing...')
          resolve()
        })
      })
    }
  },
  before: function (capabilities, specs) {
    require(`./src/${environment.environment.testProject}/support/chai-helpers`)
    fs.removeSync(config.reporterOptions.allure.outputDir)
    browser.waitForVisibleAndClick = function (element) {
      browser.waitForVisible(element)
      browser.moveToObject(element)
      browser.buttonPress()
    }
  },
  beforeScenario: function (scenario) {
    // if (browser.capabilities.platform === 'MAC') {
    //   browser.reloadSession()
    // }
  },
  afterStep: function (uri, feature, result, stepData, context) {
    console.log('STEP:' + ' ' + stepData.step.text + ' got executed and result is : ' + result.passed)
    const dir = config.screenshotsPath
    const path = require('path')
    const moment = require('moment')
    if (!result.passed) {
      const caps = browser.capabilities
      fs.ensureDirSync(dir)
      browser.saveScreenshot(dir + getFilename(stepData, caps))
      const timestamp = moment().format('YYYYMMDD-HHmmss.SSS')
      const filepath = path.join('reports/html-reports/screenshots/', timestamp + '.png')
      browser.saveScreenshot(filepath)
      process.emit('test:screenshot', filepath)
    }
  },
  afterScenario: function (uri, feature, scenario, result, sourceLocation) {
    console.log('SCENARIO:' + ' ' + scenario.name + ', ' + 'executed successfully and the execution result is: ' + result.status)
    browser.reloadSession()
  },
  onComplete: function (exitCode, config, capabilities) {
    (async () => {
      await global.reportAggregator.createReport()
    })()

    // Code to stop browserstack local after end of test
    if (environment.environment.testEnvironment === 'browserstack') {
      return new Promise(function (resolve, reject) {
        exports.bs_local.stop(function () {
          console.log('Binary stopped')
          resolve()
        })
      })
    }
  }
}

// Add device capabilities

let hasBrowser = false
let hasAndroid = false
let hasIOS = false
config.devices = environment.environment.devices

for (const device of environment.environment.devices) {
  switch (device) {
    case 'chrome':
      config.capabilities.push(chrome)
      hasBrowser = true
      break
    case 'firefox':
      config.capabilities.push(firefox)
      hasBrowser = true
      break
    case 'safari':
      config.capabilities.push(safari)
      hasBrowser = true
      break
    case 'ie':
      config.capabilities.push(ie)
      hasBrowser = true
      break
    case 'ios':
      config.capabilities.push(ios)
      hasIOS = true
      break
    case 'android':
      config.capabilities.push(android)
      hasAndroid = true
      break
    case 'android-web':
      config.capabilities.push(android)
      hasAndroid = true
      hasBrowser = true
      break
    case 'ios-safari':
      config.capabilities.push(ios)
      hasIOS = true
      hasBrowser = true
      break
    case 'ios-chrome':
      config.capabilities.push(ios)
      hasIOS = true
      hasBrowser = true
      break
    default:
      throw new Error(`Device '${device}' is not supported`)
  }
}

// We are running the services locally, therefore we need to add additional configuration
if (hasBrowser && (!(hasAndroid) || !(hasIOS))) {
  config.services.push('selenium-standalone')
  // to push browserstack config into services
  if (environment.environment.testEnvironment === 'browserstack') {
    config.services.push('browserstack')
  }
}
if (hasAndroid || (environment.environment.targetEnvironment === 'android-web')) {
  // to push browserstack config into services
  if (environment.environment.testEnvironment === 'browserstack') {
    config.services.push('browserstack')
  }
}
if (hasIOS || (environment.environment.targetDevice === 'ios-safari' || environment.environment.targetDevice === 'ios-chrome')) {
  // to push browserstack config into services
  if (environment.environment.testEnvironment === 'browserstack') {
    config.services.push('browserstack')
  }
}

if (hasBrowser) {
  switch (environment.environment.testProject) {
    case 'acm-content-hub': stgUrl = 'https://content-stg.eco.astro.com.my/'
      prodUrl = 'https://content.astro.com.my/'
      devUrl = 'https://content-dev.eco.astro.com.my/'
      break
    case 'acm' : stgUrl = 'https://stg-netizenacm.pink.cat/home'
      prodUrl = 'https://www.astro.com.my/'
      break
    case 'acm-shop' : stgUrl = 'https://vortals.webflow.io/shop'
      devUrl = 'https://astro-shop.webflow.io/'
      break
  }
  config.production = {
    portalUrl: environment.environment.testEnvironmentUrl || prodUrl,
    waitTimeout: 20000
  }
  config.dev = {
    portalUrl: environment.environment.testEnvironmentUrl || devUrl,
    waitTimeout: 20000
  }
  config.staging = {
    portalUrl: environment.environment.testEnvironmentUrl || stgUrl,
    waitTimeout: 20000
  }
} else {
  config.cucumberOpts.tags.push('~@web')
}

if (environment.environment.debug) {
  console.info('Resulting configuration:')
  console.info(config)
  console.info('\n')
}

// screenshot file name generation
const getFilename = (data, caps) => {
  const sanitize = (caps) => {
    if (!caps) {
      return ''
    }

    let result

    if (caps.deviceName !== undefined) {
      result = [sanitizeString(caps.deviceName), sanitizeString(caps.platformName), sanitizeString(caps.platformVersion), sanitizeString(caps.app)]
    } else {
      result = [sanitizeString(caps.browserName), sanitizeString(caps.version), sanitizeString(caps.platform)]
    }

    result = result.filter(n => n !== undefined && n !== '')
    return result.join('.')
  }

  const sanitizeString = function (str) {
    if (!str) {
      return ''
    }
    return String(str).replace(/^.*\/([^/]+)\/?$/, '$1').replace(/\./g, '_').replace(/\s/g, '').toLowerCase()
  }
  const fileName = data.scenario.name.replace(/ /g, '_')
  const newDate = new Date()
  const dateString = newDate.toISOString().split(/\./)[0].replace(/:/g, '-')
  const platformName = sanitize(caps)
  return platformName + '_' + fileName + '_' + dateString + '.png'
}

module.exports = { config }
