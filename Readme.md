This is a node.js based test automation framework which will be used for automating cases for web, mobile app and mobile web platforms and APIs

## Astro WebDriverIO Framework

## Installation
Install the latest version of Node.js on your system.

Clone the project and run the following command in the root directory of the project:

```sh
$ npm install
```

This installs all project dependencies

Page object definitions also exists inside the config file. All files, defining components to be tested, should end with the suffix shown below:
    exclude: [
         '*-screen.js'
    ]
Page object files are excluded from the test but are imported from and instantiated (as they contain classes) from the test files.

## Development

All commands can be found in the package.json. The most important are:

To Run tests:

$ npm test

To generate report and open the same in a browser:

$ npm run-script allure-report

**Advantages of the config file driven testing:**
All the configurations related to environment(alpha, beta, omega), country, apps (ios/android/web), and also CI can be configured at one place which would help multiple configs at different places(can be painful and also may lose track).

**Somethings which needs attention:**
I have made use of babel js compiler to achieve backward compatibility of JS with browsers  
Using chance library for random variable generation
common-errors library to enable configuring all errors at a single place
dotenv library to enable working with config files

**How to use the config file and how does the framework actually work - A brief insight** 
We have a **.env** file which is our config file
This file has many keywords or **environment variable** like what it is popularly called. 
Each of these keywords can be set to various pre-listed/agreed possible values to make our test runner run those particular scenarios in a way which the tester intends it to. 
**.env.example** is the template which actually be updated and pushed to master(github) when there is need and **.env** is for local use.
**environment.js** has the default configuration values for all the env variables which will be used by files in the framework
**src** directory serves as the main directory for the framework scripts below is the new folder structure 
**cfg** --> has the capabilities for all the clients (chrome, android, ios, ie, safari, firefox)
**features** --> Is where the features file will reside. Format for naming a feature file is **feature name.feature** for instance **auth.feature**
**step-definitions** --> This is where the steps for the feature scenarios reside. The naming convention followed for a steps file is **featureName-steps.js** for example **auth-steps.js**
**support** directory holds page objects and all supporting util files for script implementation.
here we have subfolders based on the platforms(android, ios, web) of execution.
I have made use of **appFactory** to direct the cases for each execution to the proper platform files. **app.js** and **app-factory.js** helps here
And the platform specific pages reside inside the respective platform subfolders
**screen.js** is the basepage for all web screens/pages which will be used for common functions, also can include common elements if present across multiple pages.

****************************************************
For running android tests please follow below config 
****************************************************
Local run (on your local machine):
    Download/get the android apk file 
        possible ways to do that --> 
          Build the apk file from Android Studio. Choose the build variant properly. 
          possible values --> ??
          Copy the from your mobile to your laptop(local machine where the tests are run)
          Download the proper build from CI tool:

    Launch the android emulator--> Configure and launch the android emulator from android AVD manager.
        fire the command "adb devices" (excluding the "") on the VSCode terminal and note the name of the device

    Configure .env file to include the values for below variables:
        ASTROTESTBOT_DEVICES=android
        ASTROTESTBOT_ANDROID_APP_PATH=Full Path of android-apk file location on your local system
        ASTROTESTBOT_ANDROID_DEVICE_NAME=device name from adb devices command
        ASTROTESTBOT_ANDROID_VERSION=android emulator OS version

For more insights on capabilities for Android on Appium, please go through 
http://appium.io/docs/en/writing-running-appium/caps/

Hope this notes helps with a basic understanding of the framework change and the framework itself helps us with a scalable and maintainable design.

## Initial setup of webdriverio project ( to create a new project from scratch only)

Install node.js (latest version)
    **package.json setup** 
    package.json is the configuration setup file which is referred by Node for all the project related dependencies and config scripts. 
    This is the driver for all the project based dependency settings.
    **NPM** - Node Package Manager (NPM is used to manage library installation and deletion for the project)
    command --> npm init [-y]  // -y specifies that you wish to setup package.json by answering "yes" as default to all the config queries package.json has for your project.
    if -y is omitted, then you need to choose proper responses for each of config questions package.json would need for configuration.
    Once the command npm init is fired, the resulting terminal looks like below
    This utility will walk you through creating a package.json file. It only covers the most common items, and tries to guess sensible defaults.
    See npm help json for definitive documentation on these fields and exactly what they do.
    Use npm install <pkg> afterwards to install a package and save it as a dependency in the package.json file.
    Press ^C at any time to quit.

Note: A file, package-lock.json is by default generated post package.json creation. do not ever update this file for any reason.

**install webdriverio**
npm i --save-dev @wdio/cli --> This will install webdriverio CLIs library
a new folder node_modules is created with this command and entries for @wdio can be seen in the folder

**To generate webdriverio config file fire the below command**
./node_modules/.bin/wdio config [-y] ; -y is optional as mentioned earlier for package.json generation and answer to all the config queries for the setup
post this, you can start with adding features, step-definitions, functions on pages as per the scenarios

**For mac book only**
install homebrew mkdir homebrew && curl -L https://github.com/Homebrew/brew/tarball/master | tar xz --strip 1 -C homebrew
install carthage brew update brew install carthage

## Set JAVA_HOME and ANDROID_HOME
**ANDROID_HOME**
Open command prompt.
Type - echo export "ANDROID_HOME=/Users/yourName/Library/Android/sdk" >> ~/.bash_profile. Thats's it.
Close your terminal.
Open it again.
Type - echo $ANDROID_HOME to check if the home is set.

**JAVA_HOME**
Open Terminal.
Confirm you have JDK by typing “which java”.
Check you have the needed version of Java, by typing “java -version”.
Set JAVA_HOME using this command in Terminal: export JAVA_HOME=/Library/Java/Home.
echo $JAVA_HOME on Terminal to confirm the path.

Set /bin to PATH for Java and Android:
export PATH=$ANDROID_HOME/platform-tools:$PATH
export PATH=$ANDROID_HOME/tools:$PATH
export PATH=$JAVA_HOME/bin:$PATH

## appium-doctor command helps check all the necessary appium config is done properly possible errors from appium-doctor command on MAC 
1. Xcode not installed
  Try xcode-select -p command on the terminal; If it does not show this: /Applications/Xcode.app/Contents/Developer, then there could be issues with xcode config. 
  Try below solution solution: 
  sudo xcode-select -s /Applications/Xcode.app/Contents/Developer 
2. JAVA_HOME not set error solution: 
  Manually configure the JAVA_HOME in the .bash_profile file and try again
  Once the appium-doctor gives all green, go ahead with the automation scripting

## For Mobile web automation, please follow below config

**Android**
1. Download the latest stable chrome browser apk from here: https://www.apkmirror.com/apk/google-inc/chrome/
2. Install this version on the emulator before starting automation
3. Make sure chromedriver (compatible with the above chrome browser version is installed in your project directory under node_modules).
4. While executing the script on real device comment out "udid" capability from android.js (under cfg/capabilities folder)
5. Make sure appium is installed globally (npm install -g appium)

**ios**
1. Check your simulator/real device name by executing the below command from terminal
--> instruments -s devices 
The above command gives all the ios devices list that is known to your mac along with the devices' UDID (will be mentioned within [])
This command is similar to adb devices command for android.
2. Comment out udid from ios.js (iOS capabilities file) if you want to run ios-web tests on simulator

**Currently Safari 13 is having issues with element click**
refer this: https://github.com/webdriverio/webdriverio/issues/4565  or https://bugs.webkit.org/show_bug.cgi?id=202589 for more info

## Miscellaneous

**safari settings for automation**
  safari needs additional settings for enabling automation as below:
  Enable remote automation by going to Settings > Safari > Advanced > Remote automation. -- mobile browser
  desktop --> Safari > Develop > Allow Remote Automation

**to install a new software into emulator (Android)**

do the below (here is an example of installing chrome latest version)
First start your emulator in writable mode. Put your virtual device name after -avd parameter.
Step 1: In Terminal execute this command: $ANDROID_HOME/emulator/emulator -avd Samsung_Galaxy_S7_API_24 -writable-system
Your android emulator should now be open.
Step 2: Download the apk you want from https://www.apkmirror.com/apk/google-inc/chrome/
Mine was https://www.apkmirror.com/apk/google-inc/chrome/chrome-64-0-3282-123-release/#downloads
Step 3: Once it downloads, drag and drop that .apk file onto your emulator.
Wait a few minutes and then open Chrome on that device. It should be the new version.stem

**To check the installed packages on emulator, fire the below command from the terminal**
adb shell pm list packages 

**To check the package and activity name of an app**
Open the app for which you need the info, on the emulator/real device connected to the system
From terminal/command prompt fire the below command
adb shell dumpsys window windows | grep -E 'mCurrentFocus'
result from the above command has two parts (for example, I'm trying to find the details of chrome app on my emulator and I get the below result)
mCurrentFocus=Window{5dfc879 u0 com.android.chrome/org.chromium.chrome.browser.ChromeTabbedActivity}

the first part: com.android.chrome is the app package name
second part: org.chromium.chrome.browser.ChromeTabbedActivity is the activity name

**World.js**
In order to pass the instance variables across step defintion we have to use a cucumber utility known as world objet.
this class/object helps to build a bridge between different step defintion files so that we can transfer the state of the instance variables between step definitions. please go through the world.js class under step defintion directory for more insights of how to use

**For whatsapp chat bot**
Download site for whatsapp to install on avd: https://www.apkmirror.com/apk/whatsapp-inc/whatsapp/whatsapp-2-19-360-release/whatsapp-messenger-2-19-360-4-android-apk-download/download/
app package: com.whatsapp appActivity: com.whatsapp.HomeActivity/ com.whatsapp.Main

## When there are changes made on package.json (addition/update of the packages) 
1. Need to update the version of the package.json file.
2. The version semantics is based on npm rules which can be found here: https://flaviocopes.com/npm-semantic-versioning/
3. Run the below command once you have idea on the version change
npm version patch
git add file(s) name
git commit -m "Commit message"
git push origin master
npm publish

## Important thing. Weird error !!
If you encounter ECONREFUSED/ EADDRNOTAVAIL/ or any such error due to which Selenium is not starting up or Chrome is not starting up, always check your firewall setting. Mostly it is due to firewall restricting the proxy or the tool itself (Selenium/Appium etc) due to which you will be facing the issue.

Happy coding!!
