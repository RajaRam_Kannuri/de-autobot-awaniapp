/* eslint-disable no-undef */
/* eslint-disable sort-keys */
const environment = {
  // Basic
  debug: typeof v8debug === 'object' || process.execArgv.join().includes('--debug-brk'),
  logLevel: process.env.ASTROTESTBOT_LOG_LEVEL || 'silent',
  maxInstances: process.env.ASTROTESTBOT_MAX_INSTANCES || 10,
  testProject: process.env.ASTROTESTBOT_TEST_PROJECT || 'awani-app',
  specs: process.env.ASTROTESTBOT_SPECS,
  targetEnvironment: process.env.ASTROTESTBOT_TARGET_ENV || 'production',
  testEnvironment: process.env.ASTROTESTBOT_TEST_ENV || 'local',
  appiumVersion: process.env.ASTROTESTBOT_APPIUM_VERSION || '1.16.0',
  appiumPort: process.env.ASTROTESTBOT_APPIUM_PORT || '4723',
  appiumHost: process.env.ASTROTESTBOT_APPIUM_HOST || '0.0.0.0',
  targetDevice: process.env.ASTROTESTBOT_DEVICES || 'chrome',

  // iOS
  iosVersion: process.env.ASTROTESTBOT_IOS_VERSION || '13.3',
  iosDeviceUdid: process.env.ASTROTESTBOT_IOS_LOCAL_TEST_DEVICE_UDID || '00008030-000C594202C2802E',
  iosDevTeamId: process.env.ASTROTESTBOT_IOS_DEV_TEAM_ID || '3C9G92NBVL',
  iosSimulatorDevice: process.env.ASTROTESTBOT_IOS_SIMULATOR_DEVICE || 'iPhone 11 Pro',
  iosBuildLocalFileName: process.env.ASTROTESTBOT_IOS_BUILD_LOCAL_FILE_NAME || 'build.zip',
  iosBuildRemoteFileName: process.env.ASTROTESTBOT_IOS_BUILD_REMOTE_FILE_NAME || 'build.zip',

  // Android
  androidVersion: process.env.ASTROTESTBOT_ANDROID_VERSION || '10.0',
  androidDeviceName: process.env.ASTROTESTBOT_ANDROID_DEVICE_NAME || 'Android Emulator',
  androidBuildLocalFileName: process.env.ASTROTESTBOT_ANDROID_BUILD_LOCAL_FILE_NAME || 'build.apk',
  androidBuildRemoteFileName: process.env.ASTROTESTBOT_ANDROID_BUILD_REMOTE_FILE_NAME || 'build.apk',
  androidBuildType: process.env.ASTROTESTBOT_TARGET_BUILD_TYPE || 'release',

  // Proxy
  proxyUrl: process.env.ASTROTESTBOT_PROXY_URL,
  proxyPort: process.env.ASTROTESTBOT_PROXY_PORT,
  proxySilent: true,

  // Tags
  tags: process.env.ASTROTESTBOT_TAGS,

  // Framework
  framework: process.env.ASTROTESTBOT_FRAMEWORK || 'cucumber',

  // Timeouts
  connectionRetryTimeout: process.env.ASTROTESTBOT_CONN_RETRY_TIMEOUT || 300000,
  waitforTimeout: process.env.ASTROTESTBOT_WAITFOR_TIMEOUT || 120000,
  waitForTimeoutMin: process.env.ASTROTESTBOT_MIN_WAIT_TIME || 10000,

  // Custom environment URL
  testEnvironmentUrl: process.env.ASTROTESTBOT_TEST_ENV_URL,

  // pause value before session initialize
  longPause: process.env.ASTROTESTBOT_LONG_PAUSE || 10,
  shortPause: process.env.ASTROTESTBOT_SHORT_PAUSE || 5,

  // Slack webhook url - Default to #ASTROTESTBOT-results
  slackWebhookUrls: process.env.ASTROTESTBOT_SLACK_WEBHOOK_URL || '',

  // Slack message prefix
  slackMessagePrefix: process.env.ASTROTESTBOT_SLACK_MESSAGE_PREFIX || '',

  // Browser stack config
  bstackuser: process.env.BROWSERSTACK_USERNAME || 'astroqaqa1',
  bstackaccesskey: process.env.BROWSERSTACK_ACCESS_KEY || 'Zp1fs8pb7cKq2oW9heqx'
}

if (!environment.specs) {
  environment.specs = environment.framework === 'mocha'
    ? './specs/smoke.spec.js'
    : './features/smoke-test.feature'
}

if (environment.debug) {
  environment.proxySilent = false
  environment.maxInstances = 1
}

// Configure, on which devices we will run the tests
const supportedDevices = ['chrome', 'firefox', 'safari', 'ios', 'android', 'ie', 'android-web', 'ios-safari', 'ios-chrome']
const selectedDevices = []

// We will default to chrome, if nothing is selected
if (!process.env.ASTROTESTBOT_DEVICES) {
  selectedDevices.push('chrome')
} else {
  const desiredDevices = process.env.ASTROTESTBOT_DEVICES.split(',')
  for (const device of desiredDevices) {
    if (supportedDevices.indexOf(device) === -1) {
      throw new Error(
          `Device '${device}' is not supported by ASTROTESTBOT\n` +
          'ASTROTESTBOT_DEVICE environment variable should contain comma-separated list of devices.\n' +
          `Following devices are supported: ${supportedDevices.join('|')}\n`
      )
    }

    if (device === 'ios' && !process.env.ASTROTESTBOT_IOS_APP_PATH) {
      throw new Error('You need to specify ASTROTESTBOT_PATH_IOS to the ios application')
    }

    if (device === 'android' && !process.env.ASTROTESTBOT_ANDROID_APP_PATH) {
      throw new Error('You need to specify ASTROTESTBOT_PATH_ANDROID to the android application')
    }
    selectedDevices.push(device)
  }
}

// Defaults to chrome
environment.devices = selectedDevices

if (environment.devices.includes('chrome') || environment.devices.includes('firefox') || environment.devices.includes('safari')) {
  environment.device = 'web'
}

// Mobile options
if (process.env.ASTROTESTBOT_IOS_APP_PATH) {
  environment.iosAppPath = process.env.ASTROTESTBOT_IOS_APP_PATH
}
if (process.env.ASTROTESTBOT_IOS_TARGET_BRANCH) {
  environment.iosTargetBranch = process.env.ASTROTESTBOT_IOS_TARGET_BRANCH
}
if (process.env.ASTROTESTBOT_ANDROID_TARGET_BRANCH) {
  environment.androidTargetBranch = process.env.ASTROTESTBOT_ANDROID_TARGET_BRANCH
}
if (process.env.ASTROTESTBOT_ANDROID_APP_PATH) {
  environment.androidAppPath = process.env.ASTROTESTBOT_ANDROID_APP_PATH
}

environment.androidUdid = process.env.ASTROTESTBOT_ANDROID_UDID || '00008030-000C594202C2802E'

// export default environment
module.exports = { environment }
