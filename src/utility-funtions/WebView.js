export const CONTEXT_REF = {
    NATIVE: 'NATIVE_APP',
    WEBVIEW: 'WEBVIEW_my.com.astro.awani',
    // WEBVIEW:'WEBVIEW_[com.astro.awani]',
    // WEBVIEW:'WEBVIEW_com.google.android.gms',
};
const DOCUMENT_READY_STATE = {
    COMPLETE: 'complete',
    INTERACTIVE: 'interactive',
    LOADING: 'loading',
};

class WebView {
    /**
     * Wait for the webview context to be loaded
     *
     * By default you have `NATIVE_APP` as the current context. If a webview is loaded it will be
     * added to the current contexts and will looks something like this
     * `["NATIVE_APP","WEBVIEW_28158.2"]`
     * The number behind `WEBVIEW` can be any string
     */
    static waitForWebViewContextLoaded () {
        // const currentContexts = this.getCurrentContexts();
        // console.log("THE CURRENT CONTEXT IS:"+currentContexts[1])
        driver.waitUntil(
            () => {
                const currentContexts = this.getCurrentContexts();
                console.log("THE CURRENT CONTEXT IS:"+currentContexts[1])
                return currentContexts.length > 1 &&
                    currentContexts.find(context => context.includes(CONTEXT_REF.WEBVIEW));
            }, {
                timeout: 10000,
                timeoutMsg: 'Webview context not loaded',
                interval: 100,
            },
        );
    }

    /**
     * Switch to native or webview context
     *
     * @param {string} context should be native of webview
     */
    static switchToContext (context) {
        driver.switchContext(this.getCurrentContexts()[context === CONTEXT_REF.WEBVIEW ? 1 : 0]);
    }

    /**
     * Returns an object with the list of all available contexts
     *
     * @return {object} An object containing the list of all available contexts
     */
    static getCurrentContexts () {
        return driver.getContexts();
    }

    /**
     * Wait for the document to be full loaded
     */
    static waitForDocumentFullyLoaded () {
        driver.waitUntil(
            () => driver.execute(() => document.readyState) === DOCUMENT_READY_STATE.COMPLETE,
            {
                timeout: 15000,
                timeoutMsg: 'Website not loaded',
                interval: 100,
            },
        );
    }

    /**
     * Wait for the website in the webview to be loaded
     */
        static waitForWebsiteLoaded () {
        WebView.waitForWebViewContextLoaded()
        WebView.switchToContext(CONTEXT_REF.WEBVIEW);
        WebView.waitForDocumentFullyLoaded();
        
    }
    static switchtoNativeContext()
    {
        const currentContexts = this.getCurrentContexts();
        WebView.switchToContext(CONTEXT_REF.NATIVE)
    }
}

export default WebView;