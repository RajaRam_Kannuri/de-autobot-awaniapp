/* eslint-disable no-undef */

/** WebDriverFunctions class serves as repository to all common
 * webdriver operations. This will help in case when there is any
 * changes in webdriver function syntax we just need to change here
 * and it will affect all the places.
 * */
const assert = require('assert').strict
export default class WebDriverFunctions {
  static setValue (element, data, waitTime = 2000) {
    $(selector).waitForExist(waitTime)
    browser.waitForVisible(element, 2000)
    browser.click(element)
    browser.clearElement(element)
    browser.keys(data)
  }

  static clickElement (selector, waitTime = 2000) {
    $(selector).waitForExist(waitTime)
    $(selector).click()
  }

  static scrollToElement (selector, waitTime = 2000) {
    $(selector).waitForExist(waitTime)
    $(selector).scrollIntoView()
  }

  static hoverOverElement (selector) {
    browser.moveToObject(selector)
  }

  static getElementText (selector, waitTime = 2000) {
    $(selector).waitForExist(waitTime)
    const text = $(selector).getText()
    if (text == null || text === undefined || text === '' || text === 'Object') {
      assert.fail('No case matching ' + text + ' was found for locator:' + selector)
    }
    return $(selector).getText()
  }

  static isElementDisplayed (selector, waitTime = 2000) {
    $(selector).waitForExist(waitTime)
    const result = $(selector).isDisplayed()
    if (!result) {
      assert.fail('Not displayed element ' + result + ' for locator:' + selector)
    }
    return result
  }

  static isElementEnabled (selector, waitTime = 2000) {
    $(selector).waitForExist(waitTime)
    const result = $(selector).isEnabled()
    if (!result) {
      assert.fail('Not displayed element ' + result + ' for locator:' + selector)
    }
    return result
  }

  static getElementAttribute (selector, attribute, waitTime = 2000) {
    $(selector).waitForExist(waitTime)
    const text = $(selector).getAttribute(attribute)
    if (text == null || text === undefined || text === '' || text === 'Object') {
      assert.fail('No case matching ' + text + ' was found for locator:' + selector)
    }
    return $(selector).getAttribute(attribute)
  }

  static getCSSAttribute (selector, attribute, waitTime = 2000) {
    $(selector).waitForExist(waitTime)
    const text = $(selector).getCSSProperty(attribute)
    if (text == null || text === undefined || text === '' || text === 'Object') {
      assert.fail('No case matching ' + text + ' was found for locator:' + selector)
    }
    return JSON.stringify($(selector).getCSSProperty(attribute))
  }

  // To select a value from dropdown based on certain text
  static selectByVisibleTextInDropdown (selectDropdwon, menuItemsOptions, textToSelect) {
    browser.click(selectDropdwon)
    const menuItems = browser.elements(menuItemsOptions).value
    for (const item of menuItems) {
      const menuItem = browser.elementIdText(item.ELEMENT).value
      if (menuItem.toLowerCase().includes(textToSelect.toLowerCase())) {
        browser.elementIdClick(item.ELEMENT)
        break
      }
    }
  }

  // To get title form browser
  static getBrowserTitle (waitTime = 5000) {
    browser.pause(waitTime)
    const text = browser.getTitle()
    if (text == null || text === undefined || text === '' || text === 'Object') {
      assert.fail('Not found browser title')
    }
    return text
  }

  // this method will help us to wait any element still 30 sec if not exist if not available it will fail
  static waitForElementExists (selector, waitTimeInSec = 30) {
    let ele = false
    let count = 0
    while (!$(selector).waitForExist() && count <= waitTimeInSec) {
      browser.pause(1000)
      count++
      if (count === waitTimeInSec) {
        ele = true
      }
    }
    if (ele) {
      assert.fail('locator is taking more then 30 Sec for' + selector)
    }
  }

  static verifyText (expected, actual) {
    assert.equal(expected.toLowerCase(), actual.toLowerCase(), expected + '***but found***' + actual)
    return true
  }
}
