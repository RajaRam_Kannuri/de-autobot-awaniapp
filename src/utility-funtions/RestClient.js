import Axios from 'axios'
const filesystem = require('fs');
const data = filesystem.readFileSync('response.json', 'UTF8')
const parsedResponse = JSON.parse(data)
const assert = require('assert').strict
var jp = require('jsonpath');
export  var steps = []
var map = {};

export default class RestClient {

    get(req,param="requestParameter")
    {
            Axios.get(req)
              .then(response => {
                var data = JSON.stringify(response.data.response);
                filesystem.writeFile('response.json', data, function (err) {
                    console.log(err);
                });
                return data
              })
              .catch(err => {
                console.log('Error: ', err);
              })
 
    }

    getMap(key)
    {
         console.log("INSIDE GET MAP"+map[key])
         return map[key]
    }
    getResponseforGET(req,param="requestParameter")
    {
        this.jsonObj = get(req,param)
    }
    captureResponsewithNodeValues(jsonPath,key)
    {
        var nodeValues = jp.query(parsedResponse,jsonPath);
        console.log("THE NODE VALUES:"+nodeValues)
        nodeValues.forEach((value)=>
        {
            console.log(value)
        }
        )
        map[key]=nodeValues
        console.log("While Capturing Storing in MAP"+key+":"+map[key])
    }
}
