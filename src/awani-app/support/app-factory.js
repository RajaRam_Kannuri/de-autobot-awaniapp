import AndroidApp from './platforms/android/android-app'
import IOSApp from './platforms/ios/ios-app'
import WebApp from './platforms/web/web-app'

export default class AppFactory {
  static getApp () {
    // if (browser.isIOS || browser.isAndroid) {
    if (browser.isIOS) {
      return new IOSApp()
    }
    if (browser.isAndroid) {
      return new AndroidApp()
    }
    return new WebApp()
  }
}
