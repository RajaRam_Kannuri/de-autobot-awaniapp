const sanitize = (caps) => {
  if (!caps) {
    return ''
  }

  let result

  if (caps.deviceName) {
    result = [sanitizeString(caps.deviceName), sanitizeString(caps.platformName), sanitizeString(caps.platformVersion), sanitizeString(caps.app)]
  } else {
    result = [sanitizeString(caps.browserName), sanitizeString(caps.version), sanitizeString(caps.platform), sanitizeString(caps.app)]
  }

  result = result.filter(n => n !== undefined && n !== '')
  return result.join('.')
}

const sanitizeString = function (str) {
  if (!str) {
    return ''
  }

  return String(str)
    .replace(/^.*\/([^/]+)\/?$/, '$1')
    .replace(/\./g, '_')
    .replace(/\s/g, '')
    .toLowerCase()
}

const getFilename = (result, caps) => {
  const fileName = result.scenario.replace(/ /g, '_') + '_' + result.text.replace(/ /g, '_')
  const newDate = new Date()
  const dateString = newDate.toISOString().split(/\./)[0].replace(/:/g, '-')
  const platformName = sanitize(caps)
  return platformName + '_' + fileName + '_' + dateString + '.png'
}

export default getFilename
