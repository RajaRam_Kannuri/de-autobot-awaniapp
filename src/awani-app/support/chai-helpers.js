import chai from 'chai'
import chaiAsPromised from 'chai-as-promised'
import chaiStats from 'chai-stats'

chai.use(chaiAsPromised)
chai.use(chaiStats)
global.expect = chai.expect
chai.Should()
