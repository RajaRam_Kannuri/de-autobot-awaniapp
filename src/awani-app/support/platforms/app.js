/* eslint-disable */
import { NotImplementedError } from 'common-errors'

/** App class serves as an Interface, so you know
 * what actions are supported by the application.
 *
 * For writing the tests for desired platform, you should
 * extend it and override the functions that your platform supports.
 * */
export default class App {

  /**
   * This function is to launch the awani homepoage 
   */
  initialize() {
    throw new NotImplementedError()
  }

  /**
   * This function is to add an item from "homepage Go Shop section" into the cart
   */
  addItemToCartFromACMShop() {
    throw new NotImplementedError()
  }

  /**
   * This function is to verify the selected item is successfully added to cart
   */
  verifyItemOnGoShopCart(selectedItem) {
    throw new NotImplementedError()
  }

  /**
   * This function is to scroll to Go Shop section on homepage
   */
  scrollToGoShopSectionOnHomepage() {
    throw new NotImplementedError()
  }

  clickSeeAllDealsOnHomepage() {
    throw new NotImplementedError()
  }
  verifyAllDealsPageIsLoaded() {
    throw new NotImplementedError()
  }
  clickGoShopCard() {
    throw new NotImplementedError()
  }
  clickFindOutMore() {
    throw new NotImplementedError()
  }

  verifyGoShopPageLoaded() {
    throw new NotImplementedError()
  }
  verifyRespectivePageLoaded(pageUrl) {
    throw new NotImplementedError()
  }
  clickOnPageElement(itemName) {
    throw new NotImplementedError()
  }
  clickOnPageElement(itemName,pageName) {
    throw new NotImplementedError()
  }
  verifyElementsDisplayed(sectionName,srcPageName) {
    throw new NotImplementedError()
  }
  
  verifyElements(sectionName,srcPageName) {
    throw new NotImplementedError()
  }
  clickonPageElement(itenName,pageName)
  {
    throw new NotImplementedError() 
  }
  verifySpecifiedScreenIsLoaded(srcScreenName)
  {
    throw new NotImplementedError()
  }
  /**
   * A common function to click on a recieving element
   * @param {*} recievingElement 
   * @param {*} srcPageName 
   */
  clickOnRecievingElement(recievingElement, srcPageName) {
    throw new NotImplementedError()
  }

  verifyRelevantContentDisplayed() {
    throw new NotImplementedError()
  }

  /**
   * This function is to scroll to a specified section on a specified page
   */
  scrollToChosenSectionOnSpecifiedPage(sectionName, srcPageName) {
    throw new NotImplementedError()
  }

  /**
   * This function is to verify if the specified section is displayed on the given page
   */
  verifyChosenSectionDisplayedOnSpecifiedPage(sectionName, srcPageName) {
    throw new NotImplementedError()
  }

  /**
   * To navigate to a specified page
   */
  navigateToSpecifiedPage(navigatingPage) {
    throw new NotImplementedError()
  }

  /**
   * Verify acm homepage is loaded
   */
  verifySpecifiedPageIsLoaded(landingPage) {
    throw new NotImplementedError()
  }

  /**
   * To choose an item from a source page. The item can be a content section, a hyperlink, a check box, a radio button etc.
   */
  chooseItemFromSrcPage(item, srcPage) {
    throw new NotImplementedError()
  }

  verifyRespectiveContentPageLoaded(item) {
    throw new NotImplementedError()
  }
  verifyElementDisplayed(item,result)
  {
    throw new NotImplementedError()
  }
  verifyRespectiveSocialMediaContentPageLoaded(item) {
    throw new NotImplementedError()
  }

  /** This function is to either login/signup to any astro application */
  attemptAuth (userAction, credsType) {
    throw new NotImplementedError()
  }
  verifyTable(pageName,table)
  {
    throw new NotImplementedError()
  }
  /**
   * This function is to verify that the login is successful
   */
  verifyLoginSuccessful (result) {
    throw new NotImplementedError()
  }

  /**
   * This function is to verify that all the login page form elements 
   * are displayed
   */
  verifyAllFieldsDisplayed (result) {
    throw new NotImplementedError()
  }
  searchContent(searchText) {
    throw new NotImplementedError()
  }
  consumeRestAPI(method,jsonPath,key)
  {
    throw new NotImplementedError()
  }
  
}

