import App from '../app'
import environment from '../../../../../environment'
// to pull the url from the environment file
const config = browser.config[environment.environment.targetEnvironment]
const assert = require('assert').strict

export default class WebApp extends App {
  constructor() {
    super()
    
  }

  // Method to delete the cookies and launch the website on the desired browser
  initialize () {
    browser.deleteAllCookies()
    if (environment.environment.targetDevice !== 'chrome') {
      browser.maximizeWindow()
    }
    browser.url(config.portalUrl)
    // this is here to check GA triggers
    // browser.url('https://vortals.webflow.io/tv')
    // for (var num = 1; num < 7; num++) {
    //   $('#select-pack-button-' + num).scrollIntoView()
    //   $('#select-pack-button-' + num).click()
    //   browser.pause(6000)
    //   browser.back()
    // }
  }

  addItemToCartFromACMShop () {
    return this.homeScreen.addItemToCartFromACMShop()
  }

  scrollToGoShopSectionOnHomepage () {
    this.homeScreen.scrollToGoShopSectionOnHomepage()
  }

  verifyItemOnGoShopCart (selectedItem) {
    return this.goShopScreen.verifyItemOnGoShopCart(selectedItem)
  }

  clickSeeAllDealsOnHomepage () {
    this.homeScreen.clickSeeAllDealsOnHomepage()
  }

  verifyElements (sectionName, result, srcPageName) {
    switch (srcPageName) {
      case srcPageName:
        return this.homeScreen.verifyElementsDisplayed(sectionName, result)
      default: assert.fail('No case matching ' + srcPageName + 'was found to perform action')
    }
  }

  verifyElementDisplayed (item, result) {
    switch (item) {
      case "close option":
      case "close button":
      case "homepage":
      case "the search result":
      case 'success':
      case "my previous search text":
      case "search result":
        return this.globalSearch.verifyElementText(item)
      case item:
        return this.homeScreen.verifyElementDisplayed(item, result)
      default: assert.fail('No case matching ' + item + 'was found to perform action')
    }
  }

  verifyAllDealsPageIsLoaded () {
    return this.goShopScreen.verifyAllDealsPageIsLoaded()
  }

  clickGoShopCard () {
    this.homeScreen.clickGoShopCard()
  }

  clickFindOutMore () {
    return this.homeScreen.clickFindOutMore()
  }

  clickOnPageElement (recievingElement, srcPageName) {
    switch (recievingElement) {
      case 'search icon':
      case 'close button':
      case 'browser back button':
      case 'previous page by clicking browser back button':
      case 'item':
        this.globalSearch.clickOnPageElement(recievingElement)
        break
      case 'itemName': 
      this.homeScreen.clickOnRecievingElement(srcPageName)
        break
      case recievingElement:
        switch (srcPageName) {
          case 'acm homepage':
            this.homeScreen.clickOnRecievingElement(recievingElement)
            break
          case 'cross promo page':
          case 'base page':
             this.baseScreen.clickOnReceivingElement(recievingElement)
            break
          default: assert.fail('No case matching ' + srcPageName + 'was found to perform action')
        }
        break
      default: assert.fail('No case matching receiving element:  ' + recievingElement + ' was found')
    }
  }

  verifyGoShopPageLoaded () {
    return this.goShopScreen.verifyGoShopPageLoaded()
  }

  verifyRespectivePageLoaded (pageUrl) {
    return this.homeScreen.verifyRespectivePageLoaded(pageUrl)
  }

  verifyRelevantContentDisplayed () {
    return this.homeScreen.verifyRelevantContentDisplayed()
  }

  verifyRespectiveContentPageLoaded (page) {
    return this.homeScreen.verifyRespectiveContentPageLoaded(page)
  }

  verifyRespectiveSocialMediaContentPageLoaded (page) {
    return this.homeScreen.verifyRespectiveSocialMediaContentPageLoaded(page)
  }

  scrollToChosenSectionOnSpecifiedPage (sectionName, srcPageName) {
    switch (srcPageName) {
      case 'acm homepage':
        switch (sectionName) {
          case 'header':
          case 'footer': this.baseScreen.scrollToSpecifiedSection(sectionName)
            break
          case 'go shop section': this.scrollToGoShopSectionOnHomepage()
            break
          case 'quick links section': return this.homeScreen.verifyChosenSectionDisplayedOnSpecifiedPage(sectionName)
          case 'cross promo icon': return this.baseScreen.verifyCrossPromoIconIsVisible()
          case sectionName:
            this.homeScreen.scrollToChosenSectionOnSpecifiedPage(sectionName)
            break
          default: assert.fail('No case matching ' + sectionName + 'was found to scroll to')
        }
        break
      default: assert.fail('No case matching ' + srcPageName + 'was found to perform action')
    }
  }

  verifyChosenSectionDisplayedOnSpecifiedPage (sectionName, srcPageName) {
    switch (srcPageName) {
      case 'acm homepage':
        switch (sectionName) {
          case 'header':
          case 'footer':
          case 'productsServicesList': return this.baseScreen.verifySpecifiedSectionIsDisplayed(sectionName)

          case 'quick links': return this.homeScreen.verifyChosenSectionDisplayedOnSpecifiedPage(sectionName)
          case 'cross promo icon': return this.baseScreen.verifyCrossPromoIconIsVisible()
          case sectionName:
            return this.homeScreen.verifyChosenSectionDisplayedOnSpecifiedPage(sectionName)
          default: assert.fail('No case matching ' + sectionName + 'was found to verify')
        }
        break
      default: assert.fail('No case matching ' + srcPageName + 'was found to perform action')
    }
  }

  navigateToSpecifiedPage (navigatingPage) {
    switch (navigatingPage) {
      case 'cross promo page': this.baseScreen.navigateToSpecifiedPage(navigatingPage)
        break
      default: assert.fail('No case matching ' + navigatingPage + 'was found to navigate to')
    }
  }

  verifySpecifiedPageIsLoaded (landingPage) {
    switch (landingPage) {
      case 'acm homepage': return this.homeScreen.waitForLoaded()
      case "homepage":
        return browser.getTitle().includes("searchresult?") == false
      case 'respective details of the movie': return this.homeScreen.verifyRespectiveMovieDetailsLoaded()
      case 'Astro Content page': return this.homeScreen.verifyAstroContentPageForAstroFirstIsLoaded()
      case 'astro products page': return this.baseScreen.verifyAstroProductsPageLoaded()
      case 'go shop page':
      case 'astro account page':
      case 'TV Guide page':
      case 'Astro Broadband page':
      case 'Astro Promotions page':
      case 'Astro Rewards page':
      case 'Astro - QuickPay Page':
      case 'astro go page':
      case 'syok page':
      case 'astro awani page':
      case 'gempak page':
      case 'xuan page':
      case 'astro stadium page':
      case 'astro ulagam page':
      case 'hotspot page':
      case 'TV shop page':
      case 'acm pureplay':
      case 'BB shop page':
      case 'Ultra Box page':
      case 'astro account page afterLogin':
      case 'rojak daily page': return this.miscScreen.verifySpecifiedPageIsLoaded(landingPage)
      case 'Astro Best Content page': return this.homeScreen.verifyAstroBestContentPageForAstroBestIsLoaded()
      case 'login page': return this.baseScreen.verifyLoginPageLoaded()
      default: assert.fail('No case matching ' + landingPage + 'was found to verify')
    }
  }

  chooseItemFromSrcPage (item, srcPage) {
    switch (srcPage) {
      case 'cross promo page': this.miscScreen.chooseItemFromSrcPage(item)
        break
      case 'acm homepage':
        this.homeScreen.clickOnElement(item, srcPage)
        break
      case srcPage:
        this.homeScreen.clickOnElement(item, srcPage)
        break
      default: assert.fail('No case matching ' + srcPage + 'was found to perform action')
    }
  }

  attemptAuth (userAction, credsType) {
    this.baseScreen.attemptAuth(userAction, credsType)
  }

  verifyLoginSuccessful (result) {
    return this.baseScreen.verifyLoginSuccessful(result)
  }

  verifyAllFieldsDisplayed (result) {
    return this.baseScreen.verifyAllFieldsDisplayed(result)
  }
  searchContent (searchText) {
    return this.globalSearch.searchContent(searchText)
  }
}
