import AndroidScreen from './android-screen'

export default class SearchScreen extends AndroidScreen {
  constructor () {
    super()
    // this.activityNames = {
    // },
    // this.selectors = {
    // },
    this.elements = {
      // global selectors are on the left
      // buttons
      //Channels
      searchBar:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.EditText',
      Berita:'//androidx.appcompat.app.ActionBar.Tab[@content-desc="Berita"]/android.widget.TextView',
      cancelSearch:'//android.widget.TextView[@content-desc="cancelSearch"]',
      beritaImage:'(//android.widget.ImageView[@content-desc="beritaImage"])',
      Video:'//androidx.appcompat.app.ActionBar.Tab[@content-desc="Video"]',
      clearSearch:'//android.widget.ImageView[@content-desc="clearSearch"]',
      videoImage:'(//android.widget.ImageView[@content-desc="videoImage"])',
      beritaTitle:'(//android.widget.TextView[@content-desc="beritaTitle"])',
      beritaDate:'(//android.widget.TextView[@content-desc="beritaDate"])',
      videoTitle:'(//android.widget.TextView[@content-desc="videoTitle"])',
      videoDate: '(//android.widget.TextView[@content-desc="videoDate"])',
      Tiada:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.viewpager.widget.ViewPager/android.widget.RelativeLayout/android.widget.TextView',
      homeScreenTab:'//android.widget.FrameLayout[@content-desc="Berita"]/android.widget.ImageView',
      backButton:'//android.widget.ImageView[@content-desc="solatHeaderBackButton"]',
      settings:'//android.widget.FrameLayout[@content-desc="Lagi "]',
      grid :'//android.widget.LinearLayout[@content-desc="presentationGrid"]',
      list :'//android.widget.LinearLayout[@content-desc="presentationList"]',
      malay :'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]',
      tamil:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[4]',
      Next: '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.Button',
    }
  }

  verifySpecifiedScreenIsLoaded()
  {
    return $(this.elements.akaunSaya).isDisplayed()
  }


  clickonPageElement(elem)
  {
      console.log("*************IN SEARCH SCREEN***********")
      switch(elem)
      {
        case elem:
          $(this.elements[elem]).click()
      }
  }

  sendKeys(text)
  {
      $(this.elements.searchBar).addValue(text)
  }
  
  verifyElementsDisplayed(elem,result)
  {  
    let flag;
    let element;
    switch(result)
    {
       case 'displayingCoordinates':    
            let leftx = $(this.elements.feedImage).getLocation('x')
            let rightx = leftx+ $(this.elements.feedImage).getSize('width')
            console.log("THE LEFTX:"+leftx+"  THE RIGHT X:"+rightx)
            flag = (leftx==elem.split(',')[0])&&(rightx==elem.split(',')[1])
        break
        case 'displayingElem':    
            flag = $(this.elements[elem]).isDisplayed()
        break
        case 'displaying':
            console.log("INSIDE DISPLAYING")
            $(this.elements[elem]).waitForDisplayed(3000)
            element = $$(this.elements[elem])
            console.log("THE LENGTH IS:"+element.length>1)
            element.every(ele=>{
                flag = ele.isDisplayed()
                if(flag ==false)
                   return false
            }) 
        break
        case 'notdisplaying':
            flag = $$(this.elements[elem]).length==0
         break
        case 'displayingText': 
            var text;   
            element = $$(this.elements[elem])
            console.log("THE LENGTH IS:"+element.length>1)
            element.every(ele=>{
                flag =(!((ele.getText() === undefined) || (ele.getText() === null)))
                if(flag ==false)
                   return false
            })  
        break
        case 'displayingDate':
            element = $$(this.elements[elem])
            element.every(ele=>{
                text = ele.getText()
                var d = Date.parse(text)
                flag = !(isNaN(d))
                console.log("THE FLAG "+flag)
                if(flag==false)
                   return false
                let dateSplit = text.split(' ')
                let day = parseInt(dateSplit[0])
                let year = dateSplit[2]
                flag =!(isNaN(day))&&!(isNaN(year))
                if(flag == false)
                  return false
                console.log("AFTER NAN CHECK")
                flag = ((day>0 && day<=31)&&(year.length==2||year.length==4))
                console.log('THE DAY LENGTH IS:'
                    +day.length
                )
                if(flag ==false)
                  return false
                var month=["Jan","Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"]
                flag = month.includes(dateSplit[1])
        })
        break

    }  
    return flag; 
  }    
  
      
}

