import WebView from '../../../../utility-funtions/WebView.js';
import {CONTEXT_REF} from '../../../../utility-funtions/WebView'
// import{waitForWebsiteLoaded()} from '../../../../utility-funtions/WebView'
import HomeScreen from './home-screen.js';
let map = HomeScreen.getFeedMap()
const SELECTORS = {
    WEB_VIEW_SCREEN: browser.isAndroid
        ? '*//android.webkit.WebView'
        : '*//XCUIElementTypeWebView',
};

export default class WebViewScreen extends WebView {
    /**
     * Wait for the screen to be displayed based on Xpath
     *
     * @param {boolean} isShown
     */
    constructor () {
        super()
        // this.activityNames = {
        // },
        // this.selectors = {
        // },
        this.elements = {
          // global selectors are on the left
          // buttons
          //Channels
          getFeedTitle(elem)
          {
              return '//*[text()="'+elem+'"]'
          },
          searchBar:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.EditText',
          webView:'//*[@id="app"]//h1',
          loginWebView:'//*[@id="root"]//h4',         
          webViewTitle:'//*[@id="app"]//h1', 
          cancel:'//android.widget.ImageView[@content-desc="prayertimes"]',
        }
      }
    
    waitForWebViewIsDisplayedByXpath (elem) {
        browser.pause(5000)
        WebView.waitForWebsiteLoaded()
        let flag = $(this.elements[elem]).isDisplayed()
        return flag;
    }
    waitforElementDisplayedinWebView(elem)
    {
        console.log("INSIDE CUSTOM METHOD")
        let flag = this.waitForWebViewIsDisplayedByXpath(elem)
        
        return flag
    }
    waitforTextDisplayedinWebView(elem)
    {
        WebView.waitForWebsiteLoaded()
        console.log("THE FEED TITLE:"+map.get("feedTitle"))
        let text = $(this.elements.getFeedTitle(map.get("feedTitle"))).getText();
        return (text.includes(map.get("feedTitle"))||text.includes("NotificationsTitle"))
    }
    // storeKeyValueFromPage(action,elem)
    // {
    //     switch(action)
    //     {
    //         case 'bookMark':
                
    //     }
    // }
    clickonPageElement(elem)
    {
        WebView.switchtoNativeContext()
          $(this.elements[elem]).click()

          browser.pause(3000)
    }
  
  
}

