import { assert, Assertion } from 'chai'
import AndroidScreen from './android-screen'
import RestClient from'../../../../utility-funtions/RestClient.js'
import node from '@babel/register/lib/node';
const filesystem = require('fs');
var Swipe = require('../../../../utility-funtions/swipe.js')
var jp = require('jsonpath');
var headerTime
export default class InfoSalatScreen extends AndroidScreen {
  constructor () {
    super()
     this.restClient = new RestClient()
    // this.activityNames = {
    // },
    // this.selectors = {
    // },
    this.elements = {
      
      infosalat:'//android.widget.TextView[@content-desc="solatHeaderTitle"]',
      States:'(//android.widget.TextView[@content-desc="prayerTimesDropdownTitle"])',
      Zones:'(//android.widget.TextView[@content-desc="prayerTimesDropdownTitle"])',
      solatSelectedZone:'//android.widget.TextView[@content-desc="solatSelectedZone"]',
      solatSelectedDropdown:'//android.widget.TextView[@content-desc="solatSelectedState"]',
      closePlayer:'//android.widget.ImageView[@content-desc="closePrayerTimes"]',
      inhari:'//androidx.appcompat.app.ActionBar.Tab[@content-desc="Hari ini"]',
      esok:'//androidx.appcompat.app.ActionBar.Tab[@content-desc="Esok"]',
      prayerTimes:'(//android.widget.TextView[@content-desc="solatPrayerTime"])',
      hourFormat:'(//android.widget.TextView[@content-desc="solatPrayerHour"])',
      bellIcon: '(//android.widget.ImageView[@content-desc="solatNotificationIcon"])',
      bellIcon1:'(//android.widget.ImageView[@content-desc="solatNotificationIcon"])[1]',
      solatPrayerTitle:'//android.widget.TextView[@content-desc="solatPrayerTitle"]',
      solatCountDown:'//android.widget.TextView[@content-desc="solatPrayerCountdown"]',
      solatCompassButton:'//android.widget.ImageButton[@content-desc="solatCompassButton"]',
      solatShareButton:'//android.widget.ImageView[@content-desc="solatShareButton"]',
      solatLocationPrompt:'//android.widget.TextView[@content-desc="solatLocationPrompt"]',
      solatLocationText:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.TextView[1]',
      solatLocationSymbol:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.ImageView',
      slolatBackButton:'//android.widget.ImageView[@content-desc="solatHeaderBackButton"]',
      shareLink:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.RelativeLayout/android.widget.TextView',
      nativeApps:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.ListView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView',
      drive:'//android.widget.TextView[@text="Save to Drive"]',
      gmail:'//android.widget.TextView[@text="Gmail"]',
      copy:'//android.widget.TextView[@text="Copy to clipboard"]',
      messages:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ImageView[1]',
      gmailText:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.webkit.WebView/android.webkit.WebView/android.view.View',
      myDrive:'//android.widget.TextView[@content-desc="My Drive. Click to change."]',
      messagesText:'//android.widget.EditText',
      getZones(text)
      {
          return "$..states[?(@.name=='"+text+"')].zons[*].name"
      }
      ,
      getNativeApps(index)
      {
          return '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.ListView/android.widget.LinearLayout/android.widget.LinearLayout'+'['+index+']'+'/android.widget.TextView'
      }
      ,
    }
  }
   
  verifySpecifiedScreenIsLoaded()
  {
    return $(this.elements.infosalat).isDisplayed()
  }

  chooseItem(elem)
  {
      $(this.elements[elem]).waitForDisplayed(20000)
      let context = driver.getContext();
      console.log(context)
      $(this.elements[elem]).click()
  }
  verifyJSONResponseAndUI(elem,result,jsonPath="$."){
    let flag;
    let elemText = elem.split(":")[1]
    elem = elem.includes(":")?elem.split(":")[0]:elem
    switch(result)
    {
       case 'displayingCoordinates':    
            let leftx = $(this.elements.feedImage).getLocation('x')
            let rightx = leftx+ $(this.elements.feedImage).getSize('width')
            console.log("THE LEFTX:"+leftx+"  THE RIGHT X:"+rightx)
            flag = (leftx==elem.split(',')[0])&&(rightx==elem.split(',')[1])
        break
        case 'displaying':    
            element = $$(this.elements[elem])
            console.log("THE LENGTH IS:"+element.length>1)
            element.forEach(ele=>{
                flag =(!((ele.getText() === undefined) || (ele.getText() === null)))
                var text = ele.getText()
                console.log("THE ELEMENT TEXT:"+ele.getText())
                headerTime = text
                if(flag ==false)
                   return false
            }) 
        break
        case 'displayingElem':
          $(this.elements[elem]).waitForDisplayed(3000)
          element = $$(this.elements[elem])
          console.log("THE LENGTH IS:"+element.length>1)
          element.every(ele=>{
              flag = ele.isDisplayed()
              if(flag ==false)
                 return false
          }) 
          break
        
        case 'displayingText':
            $(this.elements[elem]).waitForDisplayed(3000)
            element = $(this.elements[elem])
            // console.log("THE LENGTH IS:"+element.length>1)
            console.log("THE ELEMENT TEXT:"+elemText)
            if(element.getText().includes(elemText))
               flag = true
        return flag
        case 'displayingsolatPrayerTitle':
          $(this.elements[elem]).waitForDisplayed(3000)
          element = $$(this.elements[elem])
          console.log("THE LENGTH IS:"+element.length>1)
          for(let i = 0;i<element.length;i++)
          {
                  var elems = this.elements[elem]
                  console.log("THE ELEM"+elems)
                  var prayer_Text = $(elems+'['+(i+1)+']').getText()  
                  console.log("THE HEADER TIME"+headerTime) 
                  if(headerTime.includes(prayer_Text.split(' ')[0]))
                  {
                    flag = true
                    break
                  }
                  else 
                    flag = false
              
          }
          return flag 
        case 'rendering':
            let element = $$(this.elements[elem])
            console.log(jsonPath)
            var data = filesystem.readFileSync('response.json', 'UTF8')
            var parsedResponse = JSON.parse(data)
            var nodeValues = jp.query(parsedResponse,jsonPath);
            nodeValues = jp.query(parsedResponse,jsonPath);
            console.log("THE NODE VALUES:"+nodeValues)
            let i = 0
            element.forEach((value)=>
            {
                var ui_State = value.getText()
                var webService_State = nodeValues[i]
                if(expect(ui_State).to.equal(webService_State))
                {
                   console.log("THE STATE IN AWANI UI:"+ui_State+" Equals STATE IN WebService:"+webService_State)
                   flag = true
                }
                else 
                {
                   flag = false
                   return flag
                }
                i++
            }
            )
            return flag
            case 'renderingZonesBasedOnState':
              this.clickonPageElement("solatSelectedDropdown")
              let stateElement = $$(this.elements.States)
              let count = 1;
              var data = filesystem.readFileSync('response.json', 'UTF8')
              var parsedResponse = JSON.parse(data)
              for(let i = 1;i<=stateElement.length;i++)
              {
                if(i>1)
                 {
                    this.clickonPageElement("solatSelectedDropdown")
                 }
                if(i==12&&count<15)
                {
                   i = 11
                   Swipe.swipeUp(0.50)
                   Swipe.swipeUp(1)
                }
                 let stateText = $(this.elements.States+'['+i+']').getText()
                 console.log("THE STATE TEXT"+stateText)
                //  browser.pause(3000)
                 $(this.elements.States+'['+i+']').click()
                 this.clickonPageElement("solatSelectedZone")
                 let zoneElement =  $$(this.elements.Zones)
                 for(let j=1;j<=zoneElement.length;j++)
                 {
                    if(j>5)
                      Swipe.swipeUp(0.50)
                    let ui_Zone = $(this.elements.Zones+'['+j+']').getText()
                    console.log("THE ZONE TEXT"+ui_Zone)
                    console.log("THE ZONE JSONPARSER:"+this.elements.getZones(stateText))
                      var nodeValues = jp.query(parsedResponse,this.elements.getZones(stateText));
                      console.log("THE JSON :"+nodeValues)
                      let webService_Zone = nodeValues[j-1]
                      console.log("THE WEBSERVICES ZONE:"+j+":"+webService_Zone)
                      if(expect(ui_Zone).to.equal(webService_Zone))
                      {
                         console.log("THE ZONE IN AWANI UI:"+ui_Zone+" Equals ZONE IN WebService:"+webService_Zone)
                         flag = true
                      }
                      else
                      {
                        flag = false
                        break
                      }
                 }
                 $(this.elements.closePlayer).click()
                 count++;
              }
             
              return flag
        case 'disabled':
          element = $(this.elements[elem])
          let elemen = $('(//android.widget.ImageView[@content-desc="solatNotificationIcon"])[2]')
          // const color = element.getCSSProperty("style")
          // console.log(color)
          if(expect(element).toBeDisabled())
             flag = true
          else flag= false
          return flag
        case 'displayingDate':
            element = $$(this.elements[elem])
            element.forEach(ele=>{
                text = ele.getText()
                if(text.includes('minit')||text.includes('jam')||text.includes('hari'))
                {
                   flag = true
                }
                else{
                  var d = Date.parse(text)
                  flag = !(isNaN(d))
                  console.log("THE FLAG "+flag)
                  if(flag==false)
                     return false
                  let dateSplit = text.split(' ')
                  let day = parseInt(dateSplit[0])
                  let year = dateSplit[2]
                  flag =!(isNaN(day))&&!(isNaN(year))
                  if(flag == false)
                    return false
                  console.log("AFTER NAN CHECK")
                  flag = ((day>0 && day<=31)&&(year.length==2||year.length==4))
                  console.log('THE DAY LENGTH IS:'
                      +day.length
                  )
                  if(flag ==false)
                    return false
                  var month=["Jan","Feb",
                  "Mar",
                  "Apr",
                  "May",
                  "Jun",
                  "Jul",
                  "Aug",
                  "Sep",
                  "Oct",
                  "Nov",
                  "Dec"]
                  flag = month.includes(dateSplit[1])
  
                }
           })
        break

    }  
    return flag; 
  }
  clickonPageElement(elem)
  {
        $(this.elements[elem]).click()
  }
  verifyTable(prayerTimes,hour,iconName,table)
  {
    console.log("INSIDE INFOSALAT TABLE")
    var flag
    var temptable = table.raw()
    var tablehash = table.hashes()
    console.table(temptable)
    
    for (let i = 0; i < temptable.length; i++) {
        switch(temptable[0][i])
        {
            case 'prayerTimes':
             for(let j = 0;j<temptable.length-1;j++)
             {
                var elem = this.elements[prayerTimes]
                console.log("THE ELEM"+elem)
                var ui_PrayerTime = $(elem+'['+(j+1)+']').getText()
                var testDataPrayerTime = tablehash[j][prayerTimes]
                if(ui_PrayerTime.includes(testDataPrayerTime))
                {
                  flag = true
                }
                else
                {
                  flag = false
                  break
                }

             }
             break
             case 'hourFormat':
                //https://www.regextester.com/104041 Regext Tester 
                const ishourRegularExpression = /((1[0-2]|0?[1-9]):([0-5][0-9]) ?([AaPp][Mm]))/;
                for(let j = 0;j<temptable.length-1;j++)
                {
                  var elem = this.elements[hour]
                  console.log("THE ELEM"+elem)
                  var ui_Prayerhour = $(elem+'['+(j+1)+']').getText()
                  ishourRegularExpression.test(ui_Prayerhour)
                  if(ishourRegularExpression.test(ui_Prayerhour))
                  {
                    console.log(ui_Prayerhour)
                    flag = true
                  }
                  else
                  {
                    flag = false
                    break
                  }
                  
                }
              break
              case 'bellIcon':
                //https://www.regextester.com/104041 Regext Tester 
                for(let j = 0;j<temptable.length-1;j++)
                {
                  var elem = this.elements[iconName]
                  console.log("THE ELEM"+elem)
                  var ui_Icon = $(elem+'['+(j+1)+']').isEnabled()
                  if(ui_Icon)
                  {
                    console.log("THE BELL ICON IS ENABLED"+ui_Icon)
                    flag = true
                  }
                  else
                  {
                    flag = false
                    break
                  }
                  
                }
                break
                case 'nativeApps':
                  for(let j = 0;j<temptable.length-1;j++)
                  {
                     var elem = this.elements[temptable[0][i]]
                     console.log("THE ELEMFOR NATIVE APPS"+elem)
                     var native_APP = $(this.elements.getNativeApps(j+1)).getText()
                     var testDataPrayerTime = tablehash[j][temptable[0][i]]
                     if(native_APP.includes(testDataPrayerTime))
                     {
                       flag = true
                     }
                     else
                     {
                       flag = false
                       break
                     }
                     
                  }
                  break

        }
      }

    return flag
  }

}
