import App from '../app'
import StartScreen from './start-screen'
import HomeScreen from './home-screen'
import ListenNowScreen from './listen-now'
import SearchScreen from './search-screen'
import LiveVideoScreen from './liveVideo-screen'
import InfoSalatScreen from './infosalat-screen'
import SettingsScreen from './settings-screen'
import environment from '../../../../../environment'
import LanguagePreference from './infosalat-screen'
import WebViewScreen from './webview-screen'
import PodCastScreen from './podcast-screen'
import AuthScreen from './auth-screen'
import NotificationsScreen from './notifications-screen'
import SolatCompassScreen from './solatCompass-screen'
import RestClient from '../../../../utility-funtions/RestClient'
var androidProps = require('../../../cfg/capabilities/android');

// to pull the url from the environment file
const config = browser.config[environment.environment.targetEnvironment]

export default class AndroidApp extends App {
  constructor () {
    super()
    this.homeScreen = new HomeScreen()
    this.startScreen = new StartScreen()
    this.languagePreference= new LanguagePreference()
    this.searchScreen = new SearchScreen()
    this.liveVideoScreen = new LiveVideoScreen()
    this.listenNowScreen = new ListenNowScreen()
    this.settingsScreen = new SettingsScreen()
    this.infoSalatScreen = new InfoSalatScreen()
    this.podCastScreen = new PodCastScreen()
    this.notificationsScreen = new NotificationsScreen()
    this.webView = new WebViewScreen()
    this.solatCompassScreen = new SolatCompassScreen()
    this.restClient = new RestClient()
    this.authScreen = new AuthScreen()
    this.initializerCounter = 1
  }

  initialize () {
    if (environment.environment.targetDevice === 'android-web') {
      console.log("INSIDE WEB")
      browser.url(config.portalUrl)
    } else {
      if (this.initializerCounter === 1) {
        console.log("INSIDE APP")
        this.initializerCounter++
      } else {
        console.log("INSIDE APP ELSE")
        browser.reset()
      }
    }
  }
  checkAppInstall()
  {
    return driver.isAppInstalled(androidProps.appPackage)
  }
  checkAppUnInstall()
  {
    console.log("INSIDE NESTED UNINSTALL")
    browser.pause(3000)
    console.log("THE APP IS UNINSTALLED"+driver.isAppInstalled(androidProps.appPackage))
    return driver.isAppInstalled(androidProps.appPackage)
  }
  appUnInstall()
  {
    driver.removeApp(androidProps.appPackage)
  }
  clickonPageElement(elem,pageName)
  {
    switch(pageName)
    {
      case 'startScreen':
        this.startScreen.clickonPageElement(elem)
        break
      case 'homeScreen':
        this.homeScreen.clickonPageElement(elem)
        break
      case 'languagePreferences':
        this.languagePreference.chooseItem(elem)
        break
      case 'listenNowScreen':
        this.listenNowScreen.clickonPageElement(elem)
        break
      case 'settingsScreen':
        this.settingsScreen.clickonPageElement(elem)
        break
      case 'searchScreen':
        this.searchScreen.clickonPageElement(elem)
        break
      case 'infoSalatScreen':
        this.infoSalatScreen.clickonPageElement(elem)
        break
      case 'podCastScreen':
        this.podCastScreen.clickonPageElement(elem)
          break
      case 'notificationsScreen':
        this.notificationsScreen.clickonPageElement(elem)
        break
      case 'solatCompassScreen':
          this.solatCompassScreen.clickonPageElement(elem)
      break
      case 'webViewScreen':
        this.webView.clickonPageElement(elem)
        break
      default: assert.fail('Invalid PageName')
    }
  }
  chooseItemFromSrcPage(elem,pageName)
    {
      switch(pageName){
        case 'homeScreen':
          console.log("HOME SCREEN LOG")
          this.homeScreen.chooseItemFromSrcPage(elem)
        break
        case 'settingsScreen':
            console.log("Settings SCREEN LOG")
            this.settingsScreen.chooseItemFromSrcPage(elem)
          break
        default: assert.fail('Invalid PageName')
      }
      
    }
    verifySpecifiedScreenIsLoaded(pageName)
    {
      switch(pageName){
        case 'infoSalatScreen':
          return this.infoSalatScreen.verifySpecifiedScreenIsLoaded()
        case 'homeScreen':
          return this.homeScreen.verifySpecifiedScreenIsLoaded()
        case 'settingsScreen':
          console.log("INSIDE SETTINGS SWITCH")
          var d = Date.parse("28 Aug 2020")
          console.log("THE PARSED DATE IS"+(d instanceof Date) && !(isNaN(d)))
          return this.settingsScreen.verifySpecifiedScreenIsLoaded()
        case 'liveVideoScreen':
          console.log("INSIDE LIVE VIDEO SWITCH")
          return this.liveVideoScreen.verifySpecifiedScreenIsLoaded()
        case 'webView':
          console.log("INSIDE WEBVIEW")
          return this.webView.waitforElementDisplayedinWebView(pageName)
        case 'loginWebView':
          console.log("INSIDE WEBVIEW")
          return this.webView.waitforElementDisplayedinWebView(pageName)
        case 'notificationsScreen':
          return this.notificationsScreen.verifySpecifiedScreenIsLoaded()
        case 'podCastScreen':
            return this.podCastScreen.verifySpecifiedScreenIsLoaded()
        case 'solatCompassScreen':
            return this.solatCompassScreen.verifySpecifiedScreenIsLoaded()
        default: assert.fail('Invalid PageName')
      }
    }
    verifyElements (elemName, result, srcPageName,jsonPath="$") {
      switch (srcPageName) {
        case 'homeScreen':
          return this.homeScreen.verifyElementsDisplayed(elemName, result,jsonPath)
        case 'searchScreen':
            return this.searchScreen.verifyElementsDisplayed(elemName, result)
        case 'webViewScreen':
            return this.webView.waitforTextDisplayedinWebView(elemName)
        case 'infoSalatScreen':
            return this.infoSalatScreen.verifyJSONResponseAndUI(elemName,result,jsonPath)
        case 'settingsScreen':
            return this.settingsScreen.verifyElementsDisplayed(elemName,result)
        case 'notificationsScreen':
            return this.notificationsScreen.verifyElementsDisplayed(elemName,result)
        case 'podCastScreen':
              console.log("INSIDE PODCAST")
            return this.podCastScreen.verifyElementsDisplayed(elemName,result)
        case 'solatCompassScreen':
            return this.solatCompassScreen.verifyElementsDisplayed(elemName,result)
        default: assert.fail('No case matching ' + srcPageName + 'was found to perform action')
      }
    }
    swipe(direction,srcPageName)
    {
      switch(srcPageName)
      {
         case 'homeScreen':
           this.homeScreen.swipe(direction)
         break
         case 'liveVideoScreen':
           this.liveVideoScreen.swipe(direction)
          break
         case 'notificationsScreen':
           this.notificationsScreen.swipe(direction)
          break
         case 'podCastScreen':
            this.podCastScreen.swipe(direction)
          break
         default : assert.fail('Invalid ScreenName')

      }
    }
    sendKeys(text,srcPageName)
    {
      console.log("INSIDE SEND KEYS")
      switch(srcPageName)
      {
        case 'search-screen':
          this.searchScreen.sendKeys(text)
        break
        default : assert.fail('Invalid ScreenName')
      }
    }
    storeKeyValueFromPage(action,elem,srcPageName)
    {
      switch(srcPageName)
      {
        case 'webView':
          this.webView.storeKeyValueFromPage(action,elem)
              
      }
    }
    verifyWebView(elem)
    {
      return this.webView.waitforElementDisplayedinWebView(elem)
    }
    consumeRestAPI(Method,Request,key="customKey")
    {
       switch(Method)
       {
         case "GET":
          this.restClient.get(Request,key)
         break
         case "Capture":
           this.infoSalatScreen.CaptureResponse(Request,key)
         break
       }
    }
    verifyTable(prayerTime,hour,iconName,screenName,table)
    {
      console.log("INSIDE VERIFY TABLE")
      console.log("INSIDE VERIFY TABLE"+screenName)
      switch(screenName)
      {
        case '"infoSalatScreen"':
          return this.infoSalatScreen.verifyTable(prayerTime,hour,iconName,table)
      }
    }
    attemptAuth (userAction, credsType) {
      this.authScreen.attemptAuth(userAction, credsType)
    }
    verifyLoginSuccessful (result) {
      return this.authScreen.verifyLoginSuccessful(result)
    }
   
}
