import AndroidScreen from './android-screen'
var Swipe = require('../../../../utility-funtions/swipe.js')
export default class LiveVideoScreen extends AndroidScreen {
  constructor () {
    super()
    // this.activityNames = {
    // },
    // this.selectors = {
    // },
    this.elements = {
      // global selectors are on the left
      // buttons
      //Channels
      liveTv:'//android.widget.TextView[@content-desc="liveTVTitle"]',
      Berita:'//androidx.appcompat.app.ActionBar.Tab[@content-desc="Berita"]/android.widget.TextView',
      tvProgramName:'//android.widget.TextView[@content-desc="liveTVProgramName"]',
      programTime:'//android.widget.TextView[@content-desc="liveTVProgramTime"]',
      shareButton:'//android.widget.FrameLayout[@content-desc="liveShareButton"]/android.widget.ImageView',
      scheduleTitle:'//android.widget.TextView[@content-desc="scheduleTitle"]',
      searchButton:'//android.widget.ImageView[@content-desc="search"]',
      cancelSearch:'//android.widget.TextView[@content-desc="cancelSearch"]',
      listofArticleNews:'(//android.widget.ImageView[@content-desc="beritaImage"])',
      Video:'//androidx.appcompat.app.ActionBar.Tab[@content-desc="Video"]',
      clearSearch:'//android.widget.ImageView[@content-desc="clearSearch"]',
      listofVideoNews:'(//android.widget.RelativeLayout[@content-desc="searchVideo"])',
      homeScreenTab:'//android.widget.FrameLayout[@content-desc="Berita"]/android.widget.ImageView',
      backButton:'//android.widget.ImageView[@content-desc="solatHeaderBackButton"]',
      settings:'//android.widget.FrameLayout[@content-desc="Lagi "]',
      grid :'//android.widget.LinearLayout[@content-desc="presentationGrid"]',
      list :'//android.widget.LinearLayout[@content-desc="presentationList"]',
      malay :'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]',
      tamil:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[4]',
      Next: '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.Button',
    }
  }

  verifySpecifiedScreenIsLoaded()
  {
    let elem = browser.waitUntil(()=>$(this.elements.liveTv).getText()==='Live TV',
        {
            timeout: 5000,
            timeoutMsg: 'expected text to be different after 5s'
        })
    return $(this.elements.liveTv).isDisplayed()
  }


  clickonPageElement(elem)
  {
      console.log("*************IN LIVE/VIDEO SCREEN***********")
      switch(elem)
      {
        case elem:
          $(this.elements[elem]).click()
      }
  }

  sendKeys(text)
  {
      $(this.elements.searchBar).addValue(text)
  }
  
  verifyElementsDisplayed(elem,result)
  {  
    let flag;
    switch(result)
    {
       case 'displayingCoordinates':    
            let leftx = $(this.elements.feedImage).getLocation('x')
            let rightx = leftx+ $(this.elements.feedImage).getSize('width')
            console.log("THE LEFTX:"+leftx+"  THE RIGHT X:"+rightx)
            flag = (leftx==elem.split(',')[0])&&(rightx==elem.split(',')[1])
        break
        case 'displayingElem':    
            flag = $(this.elements[elem]).isDisplayed()
        break
        case 'displaying':
            console.log("INSIDE DISPLAYING")
            $(this.elements[elem]).waitForDisplayed(3000)
            console.log("THE LENGTH IS:"+$$(this.elements[elem]).length>1)
            flag = $$(this.elements[elem]).length>1
        break
        case 'notdisplaying':
            flag = $$(this.elements[elem]).length==0
         break
    }  
    return flag; 
  }    
  
  swipe(direction)
  {
      switch(direction)
      {
        case "UP": 
          Swipe.swipeUp(1)       
        break
        case "left":
          Swipe.swipeUp(0.10) 
          for(let i =0;i<2;i++) 
            Swipe.swipe({x:900,y:1600},{x:130,y:1600}) 
        break
        default: assert.fail("NO Direction Mentioned for Swipe")
      }
  }
}

