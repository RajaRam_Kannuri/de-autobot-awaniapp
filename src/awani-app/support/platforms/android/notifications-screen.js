import { assert } from 'chai'
import AndroidScreen from './android-screen'
var Swipe = require('../../../../utility-funtions/swipe.js')
let feedMap = new Map()

export default class HomeScreen extends AndroidScreen {
  constructor () {
    super()
    // this.activityNames = {
    // },
    // this.selectors = {
    // },
    this.elements = {
      
        NotificationsHeaderImage:'//android.widget.ImageView[@content-desc="mainHeaderImage"]',
        listofNotifications:'(//android.widget.TextView[@content-desc="notificationTitle"])',
        listofNotificationImages:'(//android.widget.ImageView[@content-desc="notificationImage"])', 
        notificationsTime:'(//android.widget.TextView[@content-desc="notificationTime"])',
        NotificationsTitle:'(//android.widget.TextView[@content-desc="notificationTitle"])'

    }
  }
    

  clickonPageElement(elem)
  {
      switch(elem)
      {
        case 'feedTitle':
          feedMap.set(elem,$(this.elements[elem]).getText())
          $(this.elements[elem]).click()
          $(this.elements[elem]).click()
          console.log("THE VALUE OF MAP IS:"+feedMap.get(elem))
          browser.pause(5000)
          break
        case 'bookMark':
            feedMap.set(elem,$(this.elements.feedTitle).getText())
            $(this.elements[elem]).click()
            console.log("THE VALUE OF BookMark:"+feedMap.get(elem))
            browser.pause(5000)
            break
        case 'NotificationsTitle':
            feedMap.set(elem,$(this.elements.NotificationsTitle).getText())
            $(this.elements[elem]).click()
            console.log("THE VALUE OF NOTIFICATIONS:"+feedMap.get(elem))
            browser.pause(5000)
            break
        case elem:
          $(this.elements[elem]).click()
          browser.pause(5000)
          break
        default : assert.fail("NO ELEMENT IS PROVIDED")
      }
      // if(elem!='closeButton')
      // {
      //     $(this.elements[elem]).click()
      //     browser.pause(5000)
      // }
      // else{
      //   driver.back()
      // }
  }
  
  chooseItemFromSrcPage(radioStationName)
  {
    console.log("WE ARE IN CHOOSE METHOD:")
      var elems = $$(this.elements.category) 
      var text;
      let flag    
      console.log("TEXT IS:"+elems.length+"****************")
      for(let i =1;i<=elems.length;i++)
      {
          if(radioStationName.includes("Bisnes"))
          {
              console.log("INSIDE BISNES")
              if(i==1)
                Swipe.swipe({ x: 700, y:530 },{ x: 80, y:530 })
              text = $('(//android.widget.TextView[@content-desc="categoryTitle"])['+i+']').getText()  
              flag = text.includes(radioStationName)
          }
          else{
            text = $('(//android.widget.TextView[@content-desc="categoryTitle"])['+i+']').getText()
            console.log("TEXT IS:"+text+"****************")
            flag = text.includes(radioStationName)
          }
          if(flag)
          {
              console.log("TEXT IS:"+text+"****************")
              $('(//android.widget.TextView[@content-desc="categoryTitle"])['+i+']').click()
              browser.pause(3000)
              break
          }
             
      }  
      return flag; 
  }
  
  verifySpecifiedScreenIsLoaded()
  {
    return $(this.elements.homepageHeader).isDisplayed()
  }  
  verifyElementsDisplayed(elem,result)
  {  
    let flag;
    switch(result)
    {
       case 'displayingCoordinates':    
            let leftx = $(this.elements.feedImage).getLocation('x')
            let rightx = leftx+ $(this.elements.feedImage).getSize('width')
            console.log("THE LEFTX:"+leftx+"  THE RIGHT X:"+rightx)
            flag = (leftx==elem.split(',')[0])&&(rightx==elem.split(',')[1])
        break
        case 'displayingText':    
            let text = $(this.elements.TrendingNews).getText()
            console.log("THE TRENDING TEXT:"+text)
            flag = text.includes(elem)
        break
        case 'displayingElem':
          $(this.elements[elem]).waitForDisplayed(3000)
          element = $$(this.elements[elem])
          console.log("THE LENGTH IS:"+element.length>1)
          element.every(ele=>{
              flag = ele.isDisplayed()
              if(flag ==false)
                 return false
          }) 
        case 'displaying':
            let element = $$(this.elements[elem])
            console.log("THE LENGTH IS:"+element.length>1)
            element.forEach(ele=>{
                flag =(!((ele.getText() === undefined) || (ele.getText() === null)))
                console.log("THE ELEMENT TEXT:"+ele.getText())
                if(flag ==false)
                   return false
            })  
        break
        case 'displayingDate':
            element = $$(this.elements[elem])
            element.forEach(ele=>{
                text = ele.getText()
                if(text.includes('minit')||text.includes('jam')||text.includes('hari'))
                {
                   flag = true
                }
                else{
                  var d = Date.parse(text)
                  flag = !(isNaN(d))
                  console.log("THE FLAG "+flag)
                  if(flag==false)
                     return false
                  let dateSplit = text.split(' ')
                  let day = parseInt(dateSplit[0])
                  let year = dateSplit[2]
                  flag =!(isNaN(day))&&!(isNaN(year))
                  if(flag == false)
                    return false
                  console.log("AFTER NAN CHECK")
                  flag = ((day>0 && day<=31)&&(year.length==2||year.length==4))
                  console.log('THE DAY LENGTH IS:'
                      +day.length
                  )
                  if(flag ==false)
                    return false
                  var month=["Jan","Feb",
                  "Mar",
                  "Apr",
                  "May",
                  "Jun",
                  "Jul",
                  "Aug",
                  "Sep",
                  "Oct",
                  "Nov",
                  "Dec"]
                  flag = month.includes(dateSplit[1])
  
                }
           })
        break

    }  
    return flag; 
  }    
  swipe(direction)
  {
      switch(direction)
      {
        case "UP": 
          Swipe.swipeUp(1)        
          Swipe.swipeUp(0.30)
        break
        case "left":
          Swipe.swipeUp(0.20) 
          Swipe.swipeLeft(1)
        break
        default: assert.fail("NO Direction Mentioned for Swipe")
      }
  }
  verifySpecifiedScreenIsLoaded()
  {
    return $(this.elements.NotificationsHeaderImage).isDisplayed()
  }
  static getFeedMap()
  {
    return feedMap;
  }
}

