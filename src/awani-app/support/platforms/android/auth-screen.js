import AndroidScreen from './android-screen'
import HomeScreen from './home-screen'
var crypto = require('crypto')
const fs = require('fs')
const data = fs.readFileSync('src/awani-app/pack-data.json', 'UTF8')
import WebView from '../../../../utility-funtions/WebView.js';
const parsedPackData = JSON.parse(data)
const assert = require('assert').strict


export default class AuthScreen extends AndroidScreen {
  constructor () {
    super()
    // this.activityNames = {
    // },
    // this.selectors = {
    // },
    this.elements = {
      // global selectors are on the left
      // buttons
      //Channels
      username:'//input[@id="Email / Mobile Number"]',
      password:'//*[@id="Password"]',
      loginFormSubmitButton:'//span[text()=\'LOG IN\']',
      forgotPassLink: '//a[@href=\'/forgot\']',
      userCredsErrorMessageHolder: 'form p',
      userNameAfterLogin:'//android.widget.TextView[@text="Product Design"]',
      cancel:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.RelativeLayout/android.widget.ImageView'

    }
  }

  isLoginPageLoaded () {
    $(this.selectors.forgotPassLink).isDisplayed()
    return $(this.elements.forgotPassLink).isDisplayed()
  }
  verifySpecifiedScreenIsLoaded()
  {
    return $(this.elements.akaunSaya).isDisplayed()
  }
  verifyLoginSuccessful (result) {
    switch (result) {
      
      case 'successful': WebView.switchtoNativeContext()
        driver.pause(3000)
        $(this.elements.userNameAfterLogin).isDisplayed(10000)
        return $(this.elements.userNameAfterLogin).isDisplayed(5000)
      case 'LogoutButton': browser.waitUntil(() => $(this.selectors.headerLogoutButton).isDisplayed(20000))
        return $(this.selectors.headerLogoutButton).isDisplayed(5000)
      case 'failure':
        return $(this.elements.userCredsErrorMessageHolder).isExisting()
      default: assert.fail('No case matching result ' + result + 'was found')
    }
  }
  verifyElementsDisplayed(elem,result)
  {  
    let flag;
    let element;
    switch(result)
    {
       case 'displayingCoordinates':    
            let leftx = $(this.elements.feedImage).getLocation('x')
            let rightx = leftx+ $(this.elements.feedImage).getSize('width')
            console.log("THE LEFTX:"+leftx+"  THE RIGHT X:"+rightx)
            flag = (leftx==elem.split(',')[0])&&(rightx==elem.split(',')[1])
        break
        case 'displayingElem':    
            flag = $(this.elements[elem]).isDisplayed()
        break
        case 'displayingTextINMap':
            console.log("INSIDE DISPLAYING")
            $(this.elements[elem]).waitForDisplayed(3000)
            element = $$(this.elements[elem])
            console.log("THE LENGTH IS:"+element.length>1)
            element.every(ele=>{
                var text = ele.getText()
                console.log("THE:"+ HomeScreen.getFeedMap().get(elem))
                flag = HomeScreen.getFeedMap().get(elem).includes(text)
                if(flag ==false)
                   return false
            }) 
        break
        case 'displaying':
            console.log("INSIDE DISPLAYING")
            $(this.elements[elem]).waitForDisplayed(3000)
            element = $$(this.elements[elem])
            console.log("THE LENGTH IS:"+element.length>1)
            element.every(ele=>{
                var text = ele.getText()
                flag =(!((ele.getText() === undefined) || (ele.getText() === null)))
                console.log("THE ELEMENT TEXT:"+ele.getText())
                if(flag ==false)
                   return false
            }) 
        break
        case 'notdisplaying':
            flag = $$(this.elements[elem]).length==0
         break
        case 'displayingText': 
            var text;   
            element = $$(this.elements[elem])
            console.log("THE LENGTH IS:"+element.length>1)
            element.every(ele=>{
                flag =(!((ele.getText() === undefined) || (ele.getText() === null)))
                if(flag ==false)
                   return false
            })  
        break
        case 'displayingDate':
            element = $$(this.elements[elem])
            element.every(ele=>{
                text = ele.getText()
                var d = Date.parse(text)
                flag = !(isNaN(d))
                console.log("THE FLAG "+flag)
                if(flag==false)
                   return false
                let dateSplit = text.split(' ')
                let day = parseInt(dateSplit[0])
                let year = dateSplit[2]
                flag =!(isNaN(day))&&!(isNaN(year))
                if(flag == false)
                  return false
                console.log("AFTER NAN CHECK")
                flag = ((day>0 && day<=31)&&(year.length==2||year.length==4))
                console.log('THE DAY LENGTH IS:'
                    +day.length
                )
                if(flag ==false)
                  return false
                var month=["Jan","Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"]
                flag = month.includes(dateSplit[1])
        })
        break

    }  
    return flag; 
  }    


  clickonPageElement(elem)
  {
      console.log("*************IN SETTINGS SCREEN***********")
      switch(elem)
      {
        case elem:
          $(this.elements[elem]).click()
      }
  }
  attemptAuth (userAction, credsType) {

    const iv = Buffer.alloc(16, 0);
    const userKey = crypto.createDecipher('aes-128-cbc', 'mypassword')
    const passKey = crypto.createDecipher('aes-128-cbc', 'mypassword')
    let user = ''
    let pass = ''
    const creds = []
    switch (userAction) {
      case 'login': 
      switch (credsType) {
        case 'valid email' :
        case 'valid':
          console.log("INSIDE VALID")
          user = userKey.update(parsedPackData.authentication_modes.email.valid_usr, 'hex', 'utf8')
          user += userKey.final('utf8')
          pass = passKey.update(parsedPackData.authentication_modes.email.valid_pass, 'hex', 'utf8')
          pass += passKey.final('utf8')
          creds.push(user, pass)
          this.loginOrSignup(creds, userAction)
          break
        case 'valid mobile': user = userKey.update(parsedPackData.authentication_modes.mobile.valid_usr, 'hex', 'utf8')
          user += userKey.final('utf8')
          pass = passKey.update(parsedPackData.authentication_modes.email.valid_pass, 'hex', 'utf8')
          pass += passKey.final('utf8')
          creds.push(user, pass)
          this.loginOrSignup(creds, userAction)
          break
        case 'invalid email': user = parsedPackData.authentication_modes.email.invalid_usr
          pass = parsedPackData.authentication_modes.email.valid_pass
          creds.push(user, pass)
          this.loginOrSignup(creds, userAction)
          break
        case 'invalid mobile': user = parsedPackData.authentication_modes.mobile.invalid_usr
          pass = parsedPackData.authentication_modes.mobile.valid_pass
          creds.push(user, pass)
          this.loginOrSignup(creds, userAction)
          break
        case 'invalid creds':
          user = parsedPackData.authentication_modes.email.invalid_usr
          pass = parsedPackData.authentication_modes.email.invalid_pass
          creds.push(user, pass)
          this.loginOrSignup(creds, userAction)
          break
        default: assert.fail('No case matching credential type ' + credsType + 'was found')
      }
      break
    } 

  }
  loginOrSignup (creds, mode) {
    // if (mode !== 'login:My Account') {
    //   this.menuScreen.openLoginPage()
    // }
      switch (mode) {
        case 'login':
        case 'login:My Account': 
          console.log("INSIDE LOG")
          $(this.elements.username, creds[0]).addValue(creds[0])
          $(this.elements.password, creds[1]).addValue(creds[1])
          $(this.elements.loginFormSubmitButton).click()
          break
        default: assert.fail('No case matching, mode: ' + mode + 'was found')
      }
  }
}
      
      
 

