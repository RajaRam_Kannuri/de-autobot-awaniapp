
export default class ListenNowScreen {
    constructor () {
      this.elements = {
          // global selectors are on the left
          // buttons
        // Instead of specifying every element just looped it by referring OnBoarding Radio Stations
        //   era:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.ImageView',
        //   hitz : '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.ImageView',
        //   mix : '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]/android.widget.RelativeLayout/android.widget.ImageView',
          onBoardingRadioStations:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.RelativeLayout',
          listenNow:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView',
          proceed: '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.Button',
          Next: '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.Button',
          interestial_ad:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View'
        }
    }
    verifySpecifiedScreenIsLoaded()
    {
        return $(this.elements.listenNow).isDisplayed()
    }
    clickonPageElement(radioStationName)
    {
      //Commented code for handling interstitial ads as of now interstitial ads are not reflecting so commented 
    //   if($$(this.elements.interestial_ad).length>0)
    //       $(this.elements.interestial_ad).click()
        var elems = $$(this.elements.onBoardingRadioStations) 
        var text;
        let flag    
        console.log("TEXT IS:"+elems.length+"****************")
        for(let i =1;i<=elems.length;i++)
        {
            text = $('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout['+i+']/android.widget.TextView').getText()
            console.log("TEXT IS:"+text+"****************")
            flag = text.includes(radioStationName)
            if(flag)
            {
                console.log("TEXT IS:"+text+"****************")
                var elem = $('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout['+i+']/android.widget.RelativeLayout/android.widget.ImageView')
                elem.click()
                break
            }
               
        }  
        return flag; 
    }
    verifyElementsDisplayed(radioStationName,result)
    {
        switch(result)
        {
           case 'displayingText':    
            var elems = $$(this.elements.onBoardingRadioStations) 
            var text;
            let flag    
            for(let i =1;i<=elems.length;i++)
            {
                text = $('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout['+i+']/android.widget.TextView').getText()
                flag = text.includes(radioStationName)
                if(flag)
                  break
            }  
            return flag; 
        }    
    }
  }
  