

import AndroidScreen from './android-screen'
var Swipe = require('../../../../utility-funtions/swipe.js')

export default class SolatCompassScreen extends AndroidScreen {
    constructor () {
    super()
      this.elements = {
          // global selectors are on the left
          // buttons
          kiblatTitle:'//android.widget.TextView[@content-desc="kiblatTitle"]',
          kiblatDescription:'//android.widget.TextView[@content-desc="kiblatDescription"]',
          kiblatArrowIcon:'//android.widget.ImageView[@content-desc="kiblatArrowIcon"]',
          kiblatBackButton:'//android.widget.ImageView[@content-desc="kiblatBackButton"]',
        }
    }
  
    verifySpecifiedScreenIsLoaded()
    {
      let elem = browser.waitUntil(()=>$(this.elements.kiblatTitle).getText()==='Kiblat',
          {
              timeout: 5000,
              timeoutMsg: 'expected text to be different after 5s'
          })
      return $(this.elements.kiblatTitle).isDisplayed()
    }
  
  
    clickonPageElement(elem)
    {
        console.log("*************IN LIVE/VIDEO SCREEN***********")
        switch(elem)
        {
          case elem:
            $(this.elements[elem]).click()
        }
    }
  
    sendKeys(text)
    {
        $(this.elements.searchBar).addValue(text)
    }
    
    verifyElementsDisplayed(elem,result)
    {  
      let flag;
      var element;
      switch(result)
      {
         case 'displayingCoordinates':    
              let leftx = $(this.elements.feedImage).getLocation('x')
              let rightx = leftx+ $(this.elements.feedImage).getSize('width')
              console.log("THE LEFTX:"+leftx+"  THE RIGHT X:"+rightx)
              flag = (leftx==elem.split(',')[0])&&(rightx==elem.split(',')[1])
          break
          case 'displayingElem':    
              flag = $(this.elements[elem]).isDisplayed()
          break
          case 'displaying':
            element = $(this.elements[elem])
            // console.log("THE LENGTH IS:"+element.length>1)        
                flag =(!((element.getText() === undefined) || (element.getText() === null)))
                console.log("THE ELEMENT TEXT:"+element.getText())
                if(flag ==false)
                   return false
          return flag
          case 'displayingText':
            element = $$(this.elements[elem])
            console.log("THE LENGTH IS:"+element.length>1)
            element.forEach(ele=>{
                flag =(!((ele.getText() === undefined) || (ele.getText() === null)))
                var text = ele.getText()
                console.log("THE ELEMENT TEXT:"+ele.getText())
                headerTime = text
                if(flag ==false)
                   return false
            }) 
          break
          case 'notdisplaying':
              flag = $$(this.elements[elem]).length==0
           break
      }  
      return flag; 
    }    
    
}
  