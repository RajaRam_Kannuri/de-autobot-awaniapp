import { NotImplementedError } from 'common-errors'
import Screen from '../screen'

class AndroidScreen extends Screen {
  constructor () {
    super()
    this.constants = {
      KEYCODE_ENTER: 66
    }
  }

  getAstroNativeSelectorWithId (id) {
    return `android=new UiSelector().resourceId("${browser.options.android}:id/${id}")`
  }

  getAndroidNativeSelectorWithId (id) {
    return `android=new UiSelector().resourceId("android:id/${id}")`
  }

  getAndroidGMSSelector (id) {
    return `android=new UiSelector().resourceId("com.google.android.gms:id/${id}")`
  }

  get webViewXpath () {
    return '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.webkit.WebView[1]/android.webkit.WebView[1]'
  }

  performIMEAction () {
    browser.deviceKeyEvent(this.constants.KEYCODE_ENTER)
  }

  get name () {
    throw new NotImplementedError()
  }

  get activityName () {
    throw new NotImplementedError()
  }

  get isLoaded () {
    return browser.getCurrentDeviceActivity() === this.activityName
  }

  waitForLoaded () {
    browser.waitUntil(() => {
      return this.isLoaded
    })
  }

  waitForUnloaded () {
    browser.waitUntil(() => {
      return !this.isLoaded
    })
  }
}

export default AndroidScreen
