import { assert } from 'chai'
import AndroidScreen from './android-screen'
var Swipe = require('../../../../utility-funtions/swipe.js')
import RestClient from'../../../../utility-funtions/RestClient.js'
import node from '@babel/register/lib/node';
const filesystem = require('fs');
var jp = require('jsonpath');
let feedMap = new Map()
export default class HomeScreen extends AndroidScreen {
  constructor () {
    super()
    // this.activityNames = {
    // },
    // this.selectors = {
    // },
    this.elements = {
      
      Yes:'/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]',
      No:'/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]',
      feedImage : '(//android.widget.ImageView[@content-desc="feedImage"])',
      category:'(//android.widget.TextView[@content-desc="categoryTitle"])',
      TrendingNews:'//android.widget.TextView[@text="Trending Topik"]',
      searchButton:'//android.widget.ImageView[@content-desc="search"]',
      heroImage:'//android.widget.ImageView[@content-desc="heroCoverImage"]',
      heroImageText:'//android.widget.TextView[@content-desc="heroTitle"]',
      trendingTopikImage:'(//android.widget.ImageView[@content-desc="trendingTopicImage"])',
      trendingTopikTitle:'(//android.widget.TextView[@content-desc="trendingTopicTitle"])',
      testAd:'//android.widget.TextView[@text = "Test Ad"]',
      feedImage:'(//android.widget.ImageView[@content-desc="feedImage"])',
      feedTitle:'(//android.widget.TextView[@content-desc="feedTitle"])',
      feedDate:'(//android.widget.TextView[@content-desc="feedDate"])',
      homeScreenTab:'//android.widget.FrameLayout[@content-desc="Berita"]/android.widget.ImageView',
      homepageHeader:'//android.widget.ImageView[@content-desc="beritaHeaderTitle"]',
      miniPlayer:'//android.widget.FrameLayout[@content-desc="Settings"]/android.widget.ImageView',
      closeButton:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[1]/android.widget.ImageView',
      miniPlayerToolTip: '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[2]',
      hitz : '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.ImageView',
      settings: '//android.widget.FrameLayout[@content-desc="Lagi "]',
      liveVideoTab:'//android.widget.FrameLayout[@content-desc="LIVE / Video"]',
      languagePreferences : '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.viewpager.widget.ViewPager/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout[1]',
      podCastTab:'//android.widget.FrameLayout[@content-desc="Podcast"]/android.widget.ImageView',
      NotificationTab:'//android.widget.FrameLayout[@content-desc="Notifikasi"]/android.widget.FrameLayout',
      syokHeaderImage:'//android.widget.ImageView[@content-desc="beritaHeaderTitle"]',
      InfosalatIcon:'//android.widget.ImageView[@content-desc="prayertimes"]',
      VideoHeaderImage:'//android.widget.ImageView[@content-desc="videoHeaderTitle"]',
      podcastHeaderImage:'//android.widget.ImageView[@content-desc="podcastHeaderTitle"]',
      NotificationsHeaderImage:'//android.widget.ImageView[@content-desc="mainHeaderImage"]',
      settingHeaderTitle:'//android.widget.ImageView[@content-desc="settingHeaderTitle"]',
      bookMark:'//android.widget.ImageView[@content-desc="feedBookmark"]',
      feedTitleforCategory:'(//android.widget.TextView[@content-desc="feedTitle"])[1]',
      NotificationDot:'//android.widget.FrameLayout[@content-desc="Notifikasi"]/android.widget.FrameLayout/android.view.View',
      shareLink:'//android.widget.ImageView[@content-desc="feedShare"]',
    }
  }
    

  clickonPageElement(elem)
  {
      switch(elem)
      {
        case 'feedTitle':
          feedMap.set(elem,$(this.elements[elem]).getText())
          $(this.elements[elem]).click()
          $(this.elements[elem]).click()
          console.log("THE VALUE OF MAP IS:"+feedMap.get(elem))
          browser.pause(5000)
          break
        case 'bookMark':
            feedMap.set(elem,$(this.elements.feedTitle).getText())
            $(this.elements[elem]).click()
            console.log("THE VALUE OF BookMark:"+feedMap.get(elem))
            browser.pause(5000)
            break
        case elem:
          $(this.elements[elem]).click()
          browser.pause(5000)
          break
        default : assert.fail("NO ELEMENT IS PROVIDED")
      }
      // if(elem!='closeButton')
      // {
      //     $(this.elements[elem]).click()
      //     browser.pause(5000)
      // }
      // else{
      //   driver.back()
      // }
  }
  
  chooseItemFromSrcPage(radioStationName)
  {
    console.log("WE ARE IN CHOOSE METHOD:")
      var elems = $$(this.elements.category) 
      let elemlength = elems.length;
      var text;
      let flag    
      console.log("TEXT IS:"+elems.length+"****************")
      for(let i =1;i<=elemlength;i++)
      {
          if(radioStationName.includes("Bisnes"))
          {
              console.log("********************************")
              console.log("INSIDE BISNES")
              if(i==1)
                Swipe.swipe({ x: 700, y:530 },{ x: 80, y:530 })
              text = $('(//android.widget.TextView[@content-desc="categoryTitle"])['+i+']').getText()  
              flag = text.includes(radioStationName)
          }
          else if(radioStationName.includes("Sukan"))
          {
              console.log("************************************")
              console.log("INSIDE Sukan")
              if(i==1)
                Swipe.swipe({ x: 700, y:530 },{ x: 80, y:530 })
              text = $('(//android.widget.TextView[@content-desc="categoryTitle"])['+i+']').getText() 
              console.log("THE TEXT:"+text) 
              flag = text.includes(radioStationName)
          }
          else if(radioStationName.includes("Dunia"))
          {
              console.log("************************************")
              console.log("INSIDE Dunia")
                Swipe.swipe({ x: 700, y:530 },{ x: 80, y:530 })
              elems = $$(this.elements.category) 
              text = $('(//android.widget.TextView[@content-desc="categoryTitle"])[6]').getText() 
              i = 6;
              console.log("THE TEXT:"+text+" "+i) 
              flag = text.includes(radioStationName)
          }
          else{
            text = $('(//android.widget.TextView[@content-desc="categoryTitle"])['+i+']').getText()
            console.log("TEXT IS:"+text+"****************")
            flag = text.includes(radioStationName)
          }
          if(flag)
          {
              console.log("TEXT IS:"+text+"****************")
              $('(//android.widget.TextView[@content-desc="categoryTitle"])['+i+']').click()
              browser.pause(3000)
              break
          }
             
      }  
      return flag; 
  }
  
  verifySpecifiedScreenIsLoaded()
  {
    return $(this.elements.homepageHeader).isDisplayed()
  }  
  verifyElementsDisplayed(elem,result,jsonPath="$.")
  {  
    let flag;
    switch(result)
    {
       case 'displayingCoordinates':    
            let leftx = $(this.elements.feedImage).getLocation('x')
            let rightx = leftx+ $(this.elements.feedImage).getSize('width')
            console.log("THE LEFTX:"+leftx+"  THE RIGHT X:"+rightx)
            flag = (leftx==elem.split(',')[0])&&(rightx==elem.split(',')[1])
        break
        case 'displayingText':    
            let text = $(this.elements.TrendingNews).getText()
            console.log("THE TRENDING TEXT:"+text)
            flag = text.includes(elem)
        break
        case 'displayingElem':
          $(this.elements[elem]).waitForDisplayed(3000)
          element = $$(this.elements[elem])
          console.log("THE LENGTH IS:"+element.length>1)
          element.every(ele=>{
              flag = ele.isDisplayed()
              if(flag ==false)
                 return false
          }) 
        case 'displaying':
            let element = $$(this.elements[elem])
            console.log("THE LENGTH IS:"+element.length>1)
            element.forEach(ele=>{
                flag =(!((ele.getText() === undefined) || (ele.getText() === null)))
                console.log("THE ELEMENT TEXT:"+ele.getText())
                if(flag ==false)
                   return false
            })  
        break
        case 'displayingDate':
            element = $$(this.elements[elem])
            element.forEach(ele=>{
                text = ele.getText()
                if(text.includes('minit')||text.includes('jam')||text.includes('hari'))
                {
                   flag = true
                }
                else{
                  var d = Date.parse(text)
                  flag = !(isNaN(d))
                  console.log("THE FLAG "+flag)
                  if(flag==false)
                     return false
                  let dateSplit = text.split(' ')
                  let day = parseInt(dateSplit[0])
                  let year = dateSplit[2]
                  flag =!(isNaN(day))&&!(isNaN(year))
                  if(flag == false)
                    return false
                  console.log("AFTER NAN CHECK")
                  flag = ((day>0 && day<=31)&&(year.length==2||year.length==4))
                  console.log('THE DAY LENGTH IS:'
                      +day.length
                  )
                  if(flag ==false)
                    return false
                  var month=["Jan","Feb",
                  "Mar",
                  "Apr",
                  "May",
                  "Jun",
                  "Jul",
                  "Aug",
                  "Sep",
                  "Oct",
                  "Nov",
                  "Dec"]
                  flag = month.includes(dateSplit[1])
  
                }
           })
        break
        case 'rendering':
            element = $$(this.elements[elem])
            console.log(jsonPath)
            var data = filesystem.readFileSync('response.json', 'UTF8')
            var parsedResponse = JSON.parse(data)
            var nodeValues = jp.query(parsedResponse,jsonPath);
            nodeValues = jp.query(parsedResponse,jsonPath);
            console.log("THE NODE VALUES:"+nodeValues)
            let i = 0
            element.forEach((value)=>
            {
                var ui_Text = value.getText()
                var webService_Text = nodeValues[i]
                if(expect(ui_Text).to.equal(webService_Text))
                {
                   console.log("THE STATE IN AWANI UI:"+ui_Text+" Equals STATE IN WebService:"+webService_Text)
                   flag = true
                }
                else 
                {
                   flag = false
                   return flag
                }
                i++
            }
            )
            return flag
        

    }  
    return flag; 
  }    
  swipe(direction)
  {
      switch(direction)
      {
        case "UP": 
          Swipe.swipeUp(1)        
          // Swipe.swipeUp(0.30)
        break
        case "LittleUp":
          case "UP":       
          Swipe.swipeUp(0.50)
        break
        case "left":
          Swipe.swipeUp(0.20) 
          Swipe.swipeLeft(1)
          Swipe.swipeLeft(0.20)
        break
        default: assert.fail("NO Direction Mentioned for Swipe")
      }
  }
  static getFeedMap()
  {
    return feedMap;
  }
}

