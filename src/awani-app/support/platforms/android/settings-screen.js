import AndroidScreen from './android-screen'
import HomeScreen from './home-screen'

export default class SettingsScreen extends AndroidScreen {
  constructor () {
    super()
    // this.activityNames = {
    // },
    // this.selectors = {
    // },
    this.elements = {
      // global selectors are on the left
      // buttons
      //Channels
      akaunSaya:'//android.widget.TextView[@content-desc="akaunSayaTitle"]',
      homeScreenTab:'//android.widget.FrameLayout[@content-desc="Berita"]/android.widget.ImageView',
      backButton:'//android.widget.ImageView[@content-desc="solatHeaderBackButton"]',
      settings:'//android.widget.FrameLayout[@content-desc="Lagi "]',
      grid :'//android.widget.LinearLayout[@content-desc="presentationGrid"]',
      list :'//android.widget.LinearLayout[@content-desc="presentationList"]',
      liveVideoTab:'//android.widget.FrameLayout[@content-desc="LIVE / Video"]',
      disimpan:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.viewpager.widget.ViewPager/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout[2]',
      bookMark:'(//android.widget.TextView[@content-desc="bookmarkTitle"])',
      loginButton:'//android.widget.Button[@content-desc="loginButton"]',
      settingsTitle:'//android.widget.TextView[@content-desc="tetapanAplikasiTitle"]',
      videoAutoPlay:'//android.widget.RelativeLayout[@content-desc="videoAutoMain"]/android.widget.TextView[1]',
      infoSolatSettingTitle:'//android.widget.TextView[@content-desc="settingSolatTitle"]',
      settingSolatToggle:'//android.widget.Switch[@content-desc="settingSolatToggle"]',
      NotificationTab:'//android.widget.FrameLayout[@content-desc="Notifikasi"]/android.widget.FrameLayout',
      notificationsTitle:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.viewpager.widget.ViewPager/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout[5]/android.widget.TextView',
      beritaDisplayTitle:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.viewpager.widget.ViewPager/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout[6]/android.widget.TextView',
      infoAwaniTitle:'//android.widget.TextView[@content-desc="infoAwaniTitle"]',
      aboutUs:'//android.widget.RelativeLayout[@content-desc="aboutUs"]/android.widget.TextView',
      contactUs:'//android.widget.RelativeLayout[@content-desc="contactUs"]/android.widget.TextView',
      googleEmailSubject:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.MultiAutoCompleteTextView',
      NotificationDot:'//android.widget.FrameLayout[@content-desc="Notifikasi"]/android.widget.FrameLayout/android.view.View',
      generalTerms:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.TextView',
      privacyPolicy:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.TextView',
      applicationVersion:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[1]',
      version:'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[2]',

    }
  }

  verifySpecifiedScreenIsLoaded()
  {
    return $(this.elements.akaunSaya).isDisplayed()
  }
  
  verifyElementsDisplayed(elem,result)
  {  
    let flag;
    let element;
    switch(result)
    {
       case 'displayingCoordinates':    
            let leftx = $(this.elements.feedImage).getLocation('x')
            let rightx = leftx+ $(this.elements.feedImage).getSize('width')
            console.log("THE LEFTX:"+leftx+"  THE RIGHT X:"+rightx)
            flag = (leftx==elem.split(',')[0])&&(rightx==elem.split(',')[1])
        break
        case 'displayingElem':    
            flag = $(this.elements[elem]).isDisplayed()
        break
        case 'displayingTextINMap':
            console.log("INSIDE DISPLAYING")
            $(this.elements[elem]).waitForDisplayed(3000)
            element = $$(this.elements[elem])
            console.log("THE LENGTH IS:"+element.length>1)
            element.every(ele=>{
                var text = ele.getText()
                console.log("THE:"+ HomeScreen.getFeedMap().get(elem))
                flag = HomeScreen.getFeedMap().get(elem).includes(text)
                if(flag ==false)
                   return false
            }) 
        break
        case 'displaying':
            console.log("INSIDE DISPLAYING")
            $(this.elements[elem]).waitForDisplayed(3000)
            element = $$(this.elements[elem])
            console.log("THE LENGTH IS:"+element.length>1)
            element.every(ele=>{
                var text = ele.getText()
                flag =(!((ele.getText() === undefined) || (ele.getText() === null)))
                console.log("THE ELEMENT TEXT:"+ele.getText())
                if(flag ==false)
                   return false
            }) 
        break
        case 'notdisplaying':
            flag = $$(this.elements[elem]).length==0
         break
        case 'displayingText': 
            var text;   
            element = $$(this.elements[elem])
            console.log("THE LENGTH IS:"+element.length>1)
            element.every(ele=>{
                flag =(!((ele.getText() === undefined) || (ele.getText() === null)))
                if(flag ==false)
                   return false
            })  
        break
        case 'displayingDate':
            element = $$(this.elements[elem])
            element.every(ele=>{
                text = ele.getText()
                var d = Date.parse(text)
                flag = !(isNaN(d))
                console.log("THE FLAG "+flag)
                if(flag==false)
                   return false
                let dateSplit = text.split(' ')
                let day = parseInt(dateSplit[0])
                let year = dateSplit[2]
                flag =!(isNaN(day))&&!(isNaN(year))
                if(flag == false)
                  return false
                console.log("AFTER NAN CHECK")
                flag = ((day>0 && day<=31)&&(year.length==2||year.length==4))
                console.log('THE DAY LENGTH IS:'
                    +day.length
                )
                if(flag ==false)
                  return false
                var month=["Jan","Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"]
                flag = month.includes(dateSplit[1])
        })
        break

    }  
    return flag; 
  }    


  clickonPageElement(elem)
  {
      console.log("*************IN SETTINGS SCREEN***********")
      switch(elem)
      {
        case elem:
          $(this.elements[elem]).click()
      }
  }
 
      
      
}

