export default class Screen {
  static waitForContexts (count) {
    browser.waitUntil(() => {
      try {
        return browser.contexts().value.length >= count
      } catch (e) {
        return false
      }
    })
  }

  static changeContextToNative () {
    Screen.waitForContexts(1)
    browser.context(browser.contexts().value[0])
  }

  static changeContextToWebView () {
    Screen.waitForContexts(2)
    browser.context(browser.contexts().value[1])
  }

  static waitUntilASelectorIsVisible (selectors) {
    browser.waitUntil(() => this.isEitherSelectorVisible(selectors))
  }

  static isEitherSelectorVisible (selectors) {
    let isVisibleSelector = false
    selectors.forEach((selector) => {
      if (browser.isVisible(selector)) isVisibleSelector = true
    })
    return isVisibleSelector
  }

  static waitForInvisible (selector) {
    browser.waitUntil(() => {
      return !browser.isVisible(selector)
    })
  }
}
