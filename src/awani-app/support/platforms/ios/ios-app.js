import App from '../app'
import environment from '../../../../../environment'
import WebDriverFunctions from '../../../../utility-funtions/webdriver-functions'
const assert = require('assert').strict
// to pull the url from the environment file
const config = browser.config[environment.environment.targetEnvironment]

export default class IOSApp extends App {
  constructor () {
    super()   
    this.menuScreen = new MenuScreen()
    this.authScreen = new AuthScreen() 
    this.initializerCounter = 1
  }

  // ACTIONS

  initialize () {
    if (environment.environment.targetDevice === 'ios-safari' || environment.environment.targetDevice === 'android-web' || environment.environment.targetDevice.includes('ios-safari', 'android-web')) { // environment.environment.targetDevice === 'ios-safari' || environment.environment.targetDevice === 'android-web' ||
      browser.url(config.portalUrl)
    } else if (environment.environment.targetDevice === 'ios-chrome') {
      WebDriverFunctions.setValue(this.homeScreen.selectors.chromeSearchInput, 'google')
      WebDriverFunctions.clickElement(this.homeScreen.selectors.chromeGoButton)
      WebDriverFunctions.setValue(this.homeScreen.selectors.chromeAddrBar, config.portalUrl)
      WebDriverFunctions.clickElement(this.homeScreen.selectors.chromeGoButton)
      browser.pause(3000)
    } else {
      if (this.initializerCounter === 1) {
        this.initializerCounter++
      } else {
        browser.reset()
      }
    }
  }

  clickOnPageElement (itemName, navigatingPage) {
    const sanitisedItemName = itemName.includes(':') ? itemName.split(':')[1] : itemName
    switch (sanitisedItemName) {
      case 'loginButton':
      case 'crossPromoIcon':
      case 'crossPromoCloseButton':
      case 'productsAndServices':
      case 'Packs Subscriptions':
      case 'Ultra Box':
      case 'NJOI':
      case 'Astro Broadband':
      case 'Business Postpaid':
      case 'Navigation TVGuide':
      case 'Navigation Promotions':
      case 'Navigation MyAccount':
      case 'Navigation Support':
      case 'redress your complaints':
      case 'WhatsApp':
      case 'Facebook':
      case 'Twitter':
      case 'Privacy':
      case 'Terms':
      case 'General Terms & Conditions':
      case 'Product & Services|Packs & Subscriptions':
      case 'Product & Services|NJOI Prepaid':
      case 'Product & Services|TV Guide':
      case 'Product & Services|Promotions':
      case 'Product & Services|Contact Us':
      case 'About Astro|Astro Malaysia Holdings':
      case 'About Astro|Astro Kasih' :
      case 'About Astro|Media Room' :
      case 'About Astro|Careers' :
      case 'About Astro|Business Partner':
      case 'Astro Businesses|Astro Go Shop' :
      case 'Astro Businesses|Astro Radio' :
      case 'Astro Businesses|Astro Productions' :
      case 'Astro Businesses|Rocket Fuel' :
      case 'Astro Businesses|Content Distribution' :
        this.menuScreen.clickOnPageElement(sanitisedItemName)
        break
      case 'Email': return true
      case 'itemName': switch (navigatingPage) {
        case navigatingPage:
          if (environment.environment.targetDevice === 'ios-safari' || environment.environment.targetDevice === 'ios-chrome' || environment.environment.targetDevice === 'android-web') {
            this.menuScreen.navigateToSpecifiedPage(navigatingPage)
          }
      }
      case 'hamburger menu' :
      case 'back button':
        this.globalSearch.clickOnPageElement(sanitisedItemName)
        break
      case sanitisedItemName: this.homeScreen.clickOnPageElement(sanitisedItemName)
        break

      default: assert.fail('No case matching itemName: ' + sanitisedItemName + 'was found')
    }
  }

  verifySpecifiedPageIsLoaded (landingPage) {
    switch (landingPage) {
      case 'login page': return this.authScreen.isLoginPageLoaded()
      case "homepage":
        return browser.getTitle().includes("searchresult?") == false
      case 'respective details of the movie': return this.homeScreen.verifyRespectiveMovieDetailsLoaded()
      case 'Astro Content page': return this.homeScreen.verifyAstroContentPageForAstroFirstIsLoaded()
      case 'astro products page': return this.menuScreen.verifySpecifiedCPPageLoaded(landingPage)
      case landingPage: return this.homeScreen.verifySpecifiedPageIsLoaded(landingPage)
      default: assert.fail('No case matching landing page: ' + landingPage + 'was found')
    }
  }

  verifyAllFieldsDisplayed (result) {
    switch (result) {
      case 'displayed on login page': return this.authScreen.verifyLoginFormElementsLoaded()
    }
  }

  attemptAuth (userAction, credsType) {
    this.authScreen.attemptAuth(userAction, credsType)
  }

  verifyLoginSuccessful (result) {
    return this.menuScreen.verifyLoginSuccessful(result)
  }

  navigateToSpecifiedPage (navigatingPage) {
    switch (navigatingPage) {
      case navigatingPage: this.menuScreen.navigateToSpecifiedPage(navigatingPage)
        break
      default: assert.fail('No case matching navigating page: ' + navigatingPage + 'was found')
    }
  }

  scrollToChosenSectionOnSpecifiedPage (sectionName, srcPageName) {
    switch (srcPageName) {
      case 'acm homepage':
        switch (sectionName) {
          case 'header':
          case 'footer': this.menuScreen.scrollToSpecifiedSection(sectionName)
            break
          case sectionName:
            this.homeScreen.scrollToChosenSectionOnSpecifiedPage(sectionName)
            break
          default: assert.fail('No case matching ' + sectionName + 'was found to scroll to')
        }
        break
      default: assert.fail('No case matching ' + srcPageName + 'was found to perform action')
    }
  }

  verifyChosenSectionDisplayedOnSpecifiedPage (sectionName, srcPageName) {
    switch (srcPageName) {
      case 'acm homepage':
        switch (sectionName) {
          case 'header':
          case 'footer':
          case 'crossPromoIcon':
          case 'productsAndServicesSection':
          case 'productsServicesList':
          case 'footerHeaders':
          case 'socialMediaIcons':
          case 'Network Systems Sdn Bhd':
          case 'Call us or WhatsApp ‘Hi’ to': return this.menuScreen.verifySpecifiedSectionDisplayed(sectionName)
          case sectionName:
            return this.homeScreen.verifyChosenSectionDisplayedOnSpecifiedPage(sectionName)
          default: assert.fail('No case matching ' + sectionName + 'was found to verify')
        }
        break
      default: assert.fail('No case matching ' + srcPageName + 'was found to perform action')
    }
  }

  clickFindOutMore () {
    return this.homeScreen.clickFindOutMore()
  }

  verifyRespectivePageLoaded (pageUrl) {
    return this.homeScreen.verifyRespectivePageLoaded(pageUrl)
  }

  chooseItemFromSrcPage (item, srcPage) {
    switch (srcPage) {
      case 'cross promo page':
      case srcPage: this.menuScreen.chooseItemFromSrcPage(item, srcPage)
        break
      default: assert.fail('No case matching ' + srcPage + 'was found to perform action')
    }
  }

  verifyRespectiveContentPageLoaded (page) {
    return this.homeScreen.verifyRespectiveContentPageLoaded(page)
  }

  verifyRespectiveSocialMediaContentPageLoaded (page) {
    return this.homeScreen.verifyRespectiveContentPageLoaded(page)
  }

  verifyElementDisplayed (item, result) {
    switch (item) {
      case "search bar option":
      case "the search result":
      case "my previous search text":
      case 'success':
      case "search a text after login":
      case "search result":
      case 'homepage':
        return this.globalSearch.verifyElementText(item)
      case item:
        return this.homeScreen.verifyElementDisplayed(item, result)
      default: assert.fail('No case matching ' + item + 'was found to perform action')
    }
  }
  searchContent(searchText) {
    return this.globalSearch.searchContent(searchText)
  }
  verifyElementText (item) {
    switch (item) {
      case item:
        return this.globalSearch.verifyElementText(item)
      default: assert.fail('No case matching ' + item + 'was found to perform action')
    }
  }
}