import AppFactory from '../support/app-factory'
import environment from '../../../environment'
const { Given, Then, When } = require('cucumber')
const app = AppFactory.getApp()
const assert = require('assert').strict
var expect = require('chai').expect

Given(/^I attempt to launch "(.*)"$/, (launchAction) => {
  switch (launchAction) {
    case 'awani app':
    app.initialize()
       console.log("WE ARE IN Awani APP")
       driver.setImplicitTimeout(30000)
       browser.pause(7000)
       console.log("THE CURRENT ACTIVITY:"+ driver.getCurrentActivity());
          break
    default: assert.fail(launchAction + 'page cannot be found')
  }
})

Given(/^I attempt to "(.*)"$/, (androidAction) =>
{
  switch(androidAction)
  {
    case 'checkAppInstall':
      return expect(app.checkAppInstall()).to.be.true
    case 'appUnInstall':
      app.appUnInstall()
      break;
    case 'checkAppUnInstall':
      console.log("INSIDE APP UNINSTALL")
      return expect(app.checkAppUnInstall()).to.be.false
  }
  
})



// and(/^I click on (.*)$/, (button) => {
//     switch(button){
//       case 'Proceed':
//         $('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.Button').click()
//         $('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.Button').click()
//         $('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.ImageView').click()
//         browser.pause(10000)
//       break
//     default: assert.fail(button + 'is not clickable')
//     }
// });
When(/^I am navigated to "(.*)"$/, function (landingPage) {

  switch (landingPage) {
    
    case 'homeScreen':
      return expect(app.verifySpecifiedScreenIsLoaded(landingPage)).to.be.true
    case 'infoSalatScreen':
      return expect(app.verifySpecifiedScreenIsLoaded(landingPage)).to.be.true
    case 'settingsScreen':
      return expect(app.verifySpecifiedScreenIsLoaded(landingPage)).to.be.true
    case 'liveVideoScreen':
      return expect(app.verifySpecifiedScreenIsLoaded(landingPage)).to.be.true
    case 'webView':
      return expect(app.verifySpecifiedScreenIsLoaded(landingPage)).to.be.true
    case 'loginWebView':
      return expect(app.verifySpecifiedScreenIsLoaded(landingPage)).to.be.true
    case 'webViewScreen':
      return expect(app.verifySpecifiedScreenIsLoaded(landingPage)).to.be.true
    case 'notificationsScreen':
      return expect(app.verifySpecifiedScreenIsLoaded(landingPage)).to.be.true
    case 'podCastScreen':
      return expect(app.verifySpecifiedScreenIsLoaded(landingPage)).to.be.true
    case 'solatCompassScreen':
      return expect(app.verifySpecifiedScreenIsLoaded(landingPage)).to.be.true
    default:
      assert.fail('No case matching ' + landingPage + 'was found to navigate to')
  }
})
When(/^I click on "(.*)" on "(.*)"$/, (element,pageName) => {
    app.clickonPageElement(element,pageName)
});
When(/^I choose "(.*)" from "(.*)"$/, (item, srcPage) => {
  app.chooseItemFromSrcPage(item, srcPage)
})
Then(/^I can see "(.*)" is "(.*)" on "(.*)"$/, (elemName, result, srcPageName) => {
  switch (result) {
   
    case result:
      switch (elemName) {
        case elemName:
          return expect(app.verifyElements(elemName, result, srcPageName)).to.be.true
        default: assert.fail('No case matching ' + result + ' was found')
      }
      break
    default: assert.fail('No case matching ' + result + 'was found')
  }
})

When(/^I attempt to "(.*)" the "(.*)" on "(.*)"$/, (action,elem,srcPageName) => {
  {
    app.storeKeyValueFromPage(action,elem,srcPageName)
  }

});
When(/^I Swipe "(.*)" on "(.*)"$/, (direction,srcPageName) => {
    app.swipe(direction,srcPageName)
})

Then(/^I Key in "(.*)" on "(.*)"$/, (text,srcPageName) => {
   
    app.sendKeys(text,srcPageName)

});

Given(/^I "(.*)" the request "(.*)"$/, (arg0,arg1) => {
    app.consumeRestAPI(arg0,arg1)
});
// When(/^I "(.*)" the desired response with JSON Path "(.*)" and key(.*)"$/, (method,jsonPath,key) => {
//    console.log(jsonPath)
//    app.consumeRestAPI(method,jsonPath,key)
// });

Then(/^I see "(.*)" which is "(.*)" on "(.*)" is Same as WebService JSON "(.*)"$/, (elemName,result,srcPageName,jsonPath) => {
  switch (result) {
   
    case result:
      switch (elemName) {
        case elemName:
          return expect(app.verifyElements(elemName, result, srcPageName,jsonPath)).to.be.true
        default: assert.fail('No case matching ' + result + ' was found')
      }
    default: assert.fail('No case matching ' + result + 'was found')
  }
});
Then(/^I see the following info is displayed on (.*)$/, (pageName,table) => {
    switch(pageName)
    {
        case pageName:
          console.log("THE PAGE NAME:"+pageName)
          console.log("THE TABLE"+table.rows())
          var tableArray = table.rows()
          tableArray.forEach((value)=>console.log(value))
          return expect(app.verifyTable(pageName,table)).to.be.true
        default: assert.fail('No case matching ' + pageName + ' was found')
      }
    }
  );
  // Then(/^I see the "(*)" and "(*)" and "(*)" is displayed on (.*)$/,(prayerTime, hour,iconName,screenName,table) {

   
  // });
  When(/^I see the "(.*)" and "(.*)" and "(.*)" is displayed on (.*)$/,(prayerTime,hour,iconName,screenName,table) => {
    switch (screenName) {
      case screenName:
           console.log(table.rows())
           console.log(screenName)
          return expect(app.verifyTable(prayerTime,hour,iconName,screenName,table)).to.be.true
        default: assert.fail('No case matching ' + pageName + ' was found')
    }
  });
  // then(/^I see the (.*)$/, (arg0, table) => {

  // });
Then(/^I see "(.*)" is "(.*)"$/, function (item, result) {
  item = item.includes(':') ? item.split(':')[0] : item
  switch (item) {
    /**
     * this function uses the world object saved instance variable from its previous step in homepage-steps
     * to verify the item's title in goshop's cart
     * this.getDataArray() gives back the world object saved value of the product title captured earlier
     * */
    case 'login': return expect(app.verifyLoginSuccessful(result)).to.be.true
    case 'all the necessary login form fields': return expect(app.verifyAllFieldsDisplayed(result)).to.be.true
    default: assert.fail('No case matching ' + item + ' was found')
  }
})

// When(/^I browse for "(.*)" on "(.*)"$/, (sectionName, srcPageName) => {
//   app.scrollToChosenSectionOnSpecifiedPage(sectionName, srcPageName)
// })

// When(/^I click on "(.*)" on "(.*)"$/, (itemName, srcPageName) => {
//   switch (itemName) {
//     case itemName:
//       app.clickOnPageElement(itemName, srcPageName)
//       break
//     default: assert.fail('No case matching ' + itemName + ' to click option')
//   }
// })

// When(/^I scroll down to "(.*)" on "(.*)"$/, (sectionName, srcPageName) => {
//   app.scrollToChosenSectionOnSpecifiedPage(sectionName, srcPageName)
// })

// Then(/^I can see "(.*)" is "(.*)" on "(.*)"$/, (sectionName, result, srcPageName) => {
//   switch (result) {
//     case 'displayingSectionElem':
//       switch (sectionName) {
//         case sectionName:
//           return expect(app.verifyChosenSectionDisplayedOnSpecifiedPage(sectionName, srcPageName)).to.be.true
//         default: assert.fail('No case matching ' + result + ' was found')
//       }
//       break
//     case result:
//       switch (sectionName) {
//         case sectionName:
//           if (environment.environment.targetDevice === 'ios-safari' || environment.environment.targetDevice === 'ios-chrome' || environment.environment.targetDevice === 'android-web') {
//             return expect(app.verifyChosenSectionDisplayedOnSpecifiedPage(sectionName, srcPageName)).to.be.true
//           }
//           return expect(app.verifyElements(sectionName, result, srcPageName)).to.be.true
//         default: assert.fail('No case matching ' + result + ' was found')
//       }
//       break
//     default: assert.fail('No case matching ' + result + 'was found')
//   }
// })

// Then(/^I am navigated to "(.*)"$/, function (landingPage) {
//   const pageName = landingPage.includes(':') ? landingPage.split(':')[1] : landingPage
//   landingPage = landingPage.includes(':') ? landingPage.split(':')[0] : landingPage
//   switch (landingPage) {
//     case 'respective socialmedia page':
//       return expect(app.verifyRespectiveSocialMediaContentPageLoaded(pageName)).to.be.true
//     case 'respective content page':
//       return expect(app.verifyRespectiveContentPageLoaded(pageName)).to.be.true
//     case 'respective page':
//       return expect(app.verifyRespectivePageLoaded(this.getDataArray())).to.be.true
//     case landingPage:
//       return expect(app.verifySpecifiedPageIsLoaded(landingPage)).to.be.true
//     default:
//       assert.fail('No case matching ' + landingPage + 'was found to navigate to')
//   }
// })

// When(/^I navigate to "(.*)"$/, function (navigatingPage) {
//   switch (navigatingPage) {
//     case "previous page by clicking browser back button": 
//     app.clickOnPageElement('item', navigatingPage)
//       break
//     case navigatingPage: app.clickOnPageElement('itemName', navigatingPage)
//       break
//     default: assert.fail('No case matching ' + navigatingPage + 'was found to navigate to')
//   }
// })

// When(/^I choose "(.*)" from "(.*)"$/, (item, srcPage) => {
//   app.chooseItemFromSrcPage(item, srcPage)
// })

When(/^I attempt to "(.*)" with "(.*)" credentials$/, (userAction, credsType) => {
  app.attemptAuth(userAction, credsType)
})
