/*
This file is run exactly once along with other feature files and is responsible for setting the World
Purpose of the World:
1) Isolate state between scenarios.
2) Share data between step definitions and hooks within a scenario.
Constraints:
1) State can make your steps more tightly coupled and harder to reuse.
Notes:
1) This file should be placed in the steps directory where it can be run once
2) For now, keep all data here general. Do not add restrictions on data here,
   instead, add constraints in support files you use through app.js
3) You call the World via the keyword 'this'. It reuses an already popular keyword,
   so verify 'this' is referring to the World if you receive reference errors.
4) The World constructor was made strictly synchronous in v0.8.0.
5) Do not create instances of this class, one is made at runtime
6) Do not use this custom World if you don't need to
7) While using world object do not use arrow (() =>) function, use function() instead
*/
const { setWorldConstructor } = require('cucumber')

var CustomWorld = function () {
  // Data
  this.dataArray = []
  this.data = undefined

  // Functions
  this.addData = function (newData) {
    this.dataArray.push(newData)
  }

  this.getDataArray = function () {
    return this.dataArray
  }
}

setWorldConstructor(CustomWorld)
