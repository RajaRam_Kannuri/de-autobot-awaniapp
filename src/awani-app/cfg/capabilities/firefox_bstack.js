const environment = require('../../../../environment')
module.exports = {
  browserName: 'firefox',
  firefoxProfile: {
    'plugin.state.silverlight': 2,
    'browserstack.debug': environment.environment.testEnvironment === 'browserstack',
    'browserstack.local': environment.environment.testEnvironment === 'browserstack',
    project: 'WebdriverIO Samples',
    build: 'grid'
  }
}
