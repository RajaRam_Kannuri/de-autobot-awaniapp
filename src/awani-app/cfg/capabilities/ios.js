const environment = require('../../../../environment')

module.exports = {
  app: process.env.ASTROTESTBOT_IOS_APP_PATH,
  appiumVersion: environment.environment.appiumVersion,
  autoAcceptAlerts: true,
  automationName: 'XCUITest',
  deviceName: environment.environment.iosSimulatorDevice,
  deviceOrientation: 'portrait',
  nativeInstrumentsLib: true,
  browserName: environment.environment.targetDevice === 'ios-safari' ? 'safari' : 'iPhone',
  platformName: 'iOS',
  platformVersion: environment.environment.iosVersion,
  showIosLog: true
  // needed so u can locally run the tests on real device
  // udid: environment.environment.iosDeviceUdid,
  // dev team ID - used to code sign the webdriver agent app
  // xcodeOrgId: environment.environment.iosDevTeamId,
  // xcodeSigningId: 'iPhone Developer'
}
