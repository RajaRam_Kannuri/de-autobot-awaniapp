const environment = require('../../../../environment')

module.exports = {
  app: process.env.ASTROTESTBOT_ANDROID_APP_PATH,
  appActivity: environment.environment.targetDevice === 'android' ? 'my.com.astro.awani.presentation.screens.launch.LaunchActivity' : '',
  appWaitActivity: '',
  appPackage: environment.environment.targetDevice === 'android' ? 'my.com.astro.awani' : '', // com.android.chrome
  appiumVersion: environment.environment.appiumVersion,
  automationName: 'UiAutomator2',
  // autoWebview: true,
  // browserName: 'Chrome',
  // chromedriverExecutable: `${process.cwd()}/node_modules/chromedriver/bin/chromedriver`,
  // chromedriverUseSystemExecutable: true,
  deviceName: environment.environment.androidDeviceName,
  deviceOrientation: 'portrait',
  fullReset: false,
  resetOnSessionStartOnly: true ,
  nativeInstrumentsLib: true,
  // noReset: true,
  platformName: 'Android',
  platformVersion: environment.environment.androidVersion,
  resetKeyboard: true,
  showChromedriverLog: true,
  udid: process.env.ASTROTESTBOT_ANDROID_UDID,
  autoGrantPermissions:true,
  unicodeKeyboard: true
}
