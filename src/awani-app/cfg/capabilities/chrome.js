var fs = require('fs')
var Buffer = require('buffer').Buffer
var extensionChrome = fs.readFileSync('src/awani-app/cfg/extension_1_1_0_0.crx')
module.exports = {
  browserName: 'chrome',
  'goog:chromeOptions': {
    args: [
      'start-fullscreen',
      'disable-web-security',
      'allow-running-insecure-content',
      'ignore-certificate-errors',
      'disable-notifications',
      'disable-infobars'
    ],
    excludeSwitches: ['disable-component-update'],
    extensions: [Buffer.from(extensionChrome, 'binary').toString('base64')]
  }
}
