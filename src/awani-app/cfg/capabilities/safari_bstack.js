const environment = require('../../../../environment')
module.exports = {
  browserName: 'safari',
  platform: 'MAC', // 'macOS 10.12',
  'safari:options': {
    cleanSession: true,
    'safari:useSimulator': true,
    browser_version: environment.environment.testEnvironment === 'browserstack' ? '13.0' : '',
    technologyPreview: environment.environment.testEnvironment !== 'browserstack' // downloaded Safari Technology preview since Safari 13 is having issues with click interaction. But this also didn work
  },
  'browserstack.debug': environment.environment.testEnvironment === 'browserstack',
  'browserstack.local': environment.environment.testEnvironment === 'browserstack',
  project: 'WebdriverIO Samples',
  build: 'grid'
}
