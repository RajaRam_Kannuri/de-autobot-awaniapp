const environment = require('../../../../environment')

module.exports = {
  // app: process.env.ASTROTESTBOT_IOS_APP_PATH,
  // appiumVersion: environment.environment.appiumVersion,
  // autoAcceptAlerts: true,
  browserName: environment.environment.targetDevice === 'ios-safari' ? 'safari' : 'iPhone',
  platformName: 'iOS',
  // browserstack specific config

  os_version: '13',
  device: 'iPhone 11 Pro',
  real_mobile: 'true',
  project: 'Wdio',
  build: 'ios v1',
  'browserstack.local': 'true',
  'browserstack.user': environment.environment.bstackuser,
  'browserstack.key': environment.environment.bstackaccesskey
}
