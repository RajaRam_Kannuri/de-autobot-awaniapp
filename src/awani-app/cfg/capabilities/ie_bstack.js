const environment = require('../../../../environment')
module.exports = {
  browserName: 'internet explorer',
  platform: 'Windows 10',
  'browserstack.debug': environment.environment.testEnvironment === 'browserstack',
  'browserstack.local': environment.environment.testEnvironment === 'browserstack'
}
