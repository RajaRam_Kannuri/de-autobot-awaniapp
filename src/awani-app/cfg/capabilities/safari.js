const environment = require('../../../../environment')
module.exports = {
  browserName: 'safari',
  platform: 'MAC', // 'macOS 10.12',
  'safari:options': {
    cleanSession: true,
    'safari:useSimulator': environment.environment.targetDevice === 'ios-safari',
    browser_version: environment.environment.testEnvironment === 'browserstack' ? '13.0' : '13.1.2',
    technologyPreview: environment.environment.testEnvironment !== 'browserstack' // downloaded Safari Technology preview since Safari 13 is having issues with click interaction. But this also didn work
  }
}
