const environment = require('../../../../environment')

module.exports = {
  resetKeyboard: true,
  unicodeKeyboard: true,

  // browserstack specific

'browserstack.user': 'astroqaqa1',
'browserstack.key': 'Zp1fs8pb7cKq2oW9heqx',
app:"bs://7731812b69bf24b6fccb4fb1934a7117df23644e",
'browserstack.local': false,
"browserstack.resignApp" : "false",
'browserstack.appium_version': '1.18.0',
device: 'Google Pixel 3 XL',
os_version: '9.0',
project: 'WebdriverIO AWANI',
build: 'ANDROID_AWANI',
name: 'BROWSER_STACK_TEST',

}
