Feature: Awani Mobile App Sanity feature
 #I want to test basic Sanity test for Syok Mobile App

#  Background: As a user I launch the acm homepage


Background:  As a user I launch the AWANI APP
    Given I attempt to "checkAppInstall"
    And I attempt to launch "awani app" 
    And I click on "proceed" on "startScreen"


Scenario: As a user I should be able to select the Location access to Yes and then it's navigated to Notifications Screen
    
    And I click on "Yes" on "homeScreen"
    Then I am navigated to "infoSalatScreen"

Scenario: As a User I should be able to check app Uninstall is working or Not
   And I attempt to "appUnInstall"
   Then I attempt to "checkAppUnInstall"
   
Scenario: As a user I should be able to select the Location access to No and then it's navigated to Settings Screen
    
    And I click on "No" on "homeScreen"
    Then I am navigated to "settingsScreen"


# Scenario Outline: As a user I should be able to select different Categories on Home Screen on Select the news changes to particular Category
    
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I click on "homeScreenTab" on "settingsScreen"
#     When I am navigated to "homeScreen"
#     And I "GET" the request "<RestAPI>"
#     And I choose "<category>" from "homeScreen"
#     Then I see "feedTitleforCategory" which is "rendering" on "homeScreen" is Same as WebService JSON "$..title"

#     Examples:
#         | category   | RestAPI                                                                                                                                                              |
#         | Imbasan    | https://de-api.eco.astro.com.my/topic/api/v1/all?pageSize=15&pageNumber=1&keyword=imbasan+2020&language=bm%2Cen&site=awani                                           |
#         | English    | https://de-api.eco.astro.com.my/combineFeed/api/v2?pageSize=15&pageNumber=1&sfvTags=Category%3Acategory%3Denglish&articleCategories=69&site=awani&language=bm        |
#         | Malaysia   | https://de-api.eco.astro.com.my/combineFeed/api/v2?pageSize=15&pageNumber=1&sfvTags=Category%3Acategory%3Dnational&articleCategories=1&site=awani&language=bm        |
#         | Politik    | https://de-api.eco.astro.com.my/combineFeed/api/v2?pageSize=15&pageNumber=1&sfvTags=Category%3Acategory%3Dcurrent+affairs&articleCategories=44&site=awani&language=bm|
#         | Hiburan    | https://de-api.eco.astro.com.my/combineFeed/api/v2?pageSize=15&pageNumber=1&sfvTags=Category%3Acategory%3Dentertainment&articleCategories=5&site=awani&language=bm   |
#         | Terkini    | https://de-api.eco.astro.com.my/feed/api/v1/articles?site=awani&language=bm&pageSize=45&pageNumber=1                                                    |
#         | Bisnes     | https://de-api.eco.astro.com.my/combineFeed/api/v2?pageSize=15&pageNumber=1&sfvTags=Category%3Acategory%3DBusiness&articleCategories=3&site=awani&language=bm        |
#         | Sukan      | https://de-api.eco.astro.com.my/combineFeed/api/v2?pageSize=15&pageNumber=1&sfvTags=Category%3Acategory%3Dsports&articleCategories=4&site=awani&language=bm          |
#         | Dunia      | https://de-api.eco.astro.com.my/combineFeed/api/v2?pageSize=15&pageNumber=1&sfvTags=Category%3Acategory%3Dinternational&articleCategories=2&site=awani&language=bm   |

# Scenario Outline: As a user I should be able to select list view/Grid View and in Settings Screen the Categories should display according to the Selected View
    
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     # And I click on "settings" on "homeScreen"
#     And I click on "<View>" on "settingsScreen"
#     And I click on "homeScreenTab" on "settingsScreen"
#     And I choose "English" from "homeScreen"
#     Then I can see "<BannerSize>" is "displayingCoordinates" on "homeScreen"
#     Examples:
#         | View   | BannerSize|
#         | grid   | 44,529    |
#         | list   | 44,1036   |

# Scenario: As a user I should be able to search and Display list of Article news and Video news and perform basic operation and navigate back to home Page
    
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I click on "homeScreenTab" on "settingsScreen"
#     When I click on "searchButton" on "homeScreen"
#     And I Key in "English" on "search-screen"
#     And I can see "Berita" is "displayingElem" on "searchScreen"
#     And I can see "Video" is "displayingElem" on "searchScreen"
#     And I can see "beritaImage" is "displaying" on "searchScreen"
#     And I can see "beritaTitle" is "displayingText" on "searchScreen"
#     And I can see "beritaDate" is "displayingDate" on "searchScreen"
#     And I click on "Video" on "searchScreen"
#     And I can see "videoImage" is "displaying" on "searchScreen"
#     And I can see "videoTitle" is "displayingText" on "searchScreen"
#     And I can see "videoDate" is "displayingDate" on "searchScreen"
#     And I click on "clearSearch" on "searchScreen"
#     And I can see "Berita" is "notdisplaying" on "searchScreen"
#     And I click on "cancelSearch" on "searchScreen"
#     Then I am navigated to "homeScreen"

# Scenario Outline: As a user I should be able to search with Numerics,AlphanNumerics and Special Characters
    
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I click on "homeScreenTab" on "settingsScreen"
#     When I click on "searchButton" on "homeScreen"
#     And I Key in "<searchText>" on "search-screen"
#     Then I can see "<result>" is "<action>" on "searchScreen"
#     Examples:
#         | searchText       | result              | action        |
#         | English          | beritaImage         | displaying    |
#         | Malay            | beritaImage         | displaying    |
#         | Bisnes           | beritaImage         | displaying    |
#         | Covid-19         | beritaImage         | displaying    |
#         | es               | beritaImage         | notdisplaying |
#         | 1234             | Tiada               | displayingText|
#         | @#$%             | Tiada               | displayingText|
#         | @!               | beritaImage         | notdisplaying |
#         | @!               | beritaImage         | notdisplaying |
#         | abc1             | Tiada               | displayingText|
#         | 000              | beritaImage         | displaying    |
#         | 111              | beritaImage         | displaying    |
#         | 222              | beritaImage         | displaying    |
#         | 1111             | Tiada               | displayingText|

# Scenario: As a user I should be able to swipe the Terikini Hero Banner 
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I click on "homeScreenTab" on "settingsScreen"
#     When I can see "heroImage" is "displayingElem" on "homeScreen" 
#     And I can see "heroImageText" is "displaying" on "homeScreen"
#     And I Swipe "left" on "homeScreen"
#     When I can see "heroImage" is "displayingElem" on "homeScreen" 
#     And I can see "heroImageText" is "displaying" on "homeScreen"
#     And I Swipe "left" on "homeScreen"
#     When I can see "heroImage" is "displayingElem" on "homeScreen" 
#     And I can see "heroImageText" is "displaying" on "homeScreen"
#     And I Swipe "left" on "homeScreen"
#     When I can see "heroImage" is "displayingElem" on "homeScreen" 
#     And I can see "heroImageText" is "displaying" on "homeScreen"
#     And I Swipe "left" on "homeScreen"
#     When I can see "heroImage" is "displayingElem" on "homeScreen" 
#     Then I can see "heroImageText" is "displaying" on "homeScreen"

# Scenario: As a user I should be able to Navigate to Live/Video Screen and should be able to navigate to different categories
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     When I click on "liveVideoTab" on "settingsScreen"
#     And I am navigated to "liveVideoScreen"
#     When I can see "tvProgramName" is "displayingElem" on "liveVideoScreen" 
#     And I Swipe "left" on "liveVideoScreen"

# Scenario: As a user I should be able to Navigate to Trening Topik Section
    
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I click on "homeScreenTab" on "settingsScreen"
#     And I choose "English" from "homeScreen"
#     When I Swipe "UP" on "homeScreen"
#     And I Swipe "LittleUp" on "homeScreen"
#     And I can see "testAd" is "displayingElem" on "homeScreen" 
#     And I Swipe "LittleUp" on "homeScreen"
#     And I can see "Trending Topik" is "displayingText" on "homeScreen"
#     And I can see "trendingTopikImage" is "displayingElem" on "homeScreen"
#     And I can see "trendingTopikTitle" is "displaying" on "homeScreen"
#     And I Swipe "left" on "homeScreen"
#     And I can see "trendingTopikImage" is "displayingElem" on "homeScreen"
#     And I can see "trendingTopikTitle" is "displaying" on "homeScreen"
#     And I Swipe "left" on "homeScreen"
#     And I can see "trendingTopikImage" is "displayingElem" on "homeScreen"
#     And I can see "trendingTopikTitle" is "displaying" on "homeScreen"
#     And I Swipe "left" on "homeScreen"
#     And I can see "trendingTopikImage" is "displayingElem" on "homeScreen"
#     Then I can see "trendingTopikTitle" is "displaying" on "homeScreen"


# Scenario: As a user I should be able to see the News Feed should contain Feed Title and Timing in Grid View
    
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I click on "homeScreenTab" on "settingsScreen"
#     And I choose "English" from "homeScreen"
#     And I can see "feedImage" is "displayingElem" on "homeScreen"
#     And I can see "feedTitle" is "displaying" on "homeScreen"
#     And I can see "feedDate" is "displayingDate" on "homeScreen"
#     When I Swipe "UP" on "homeScreen"
#     And I can see "feedImage" is "displayingElem" on "homeScreen"
#     And I can see "feedTitle" is "displaying" on "homeScreen"
#     And I can see "feedDate" is "displayingDate" on "homeScreen"
#     When I Swipe "UP" on "homeScreen"
#     When I Swipe "UP" on "homeScreen"
#     And I can see "feedImage" is "displayingElem" on "homeScreen"
#     And I can see "feedTitle" is "displaying" on "homeScreen"
#     Then I can see "feedDate" is "displayingDate" on "homeScreen"

# Scenario: As a user I should be able to BookMark a particular Video or Article feed in Grid Mode so that it will be displayed in Disimpan Screen
    
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I click on "list" on "settingsScreen"
#     And I click on "homeScreenTab" on "settingsScreen"
#     And I choose "English" from "homeScreen"
#     And I click on "bookMark" on "homeScreen"
#     And I click on "settings" on "homeScreen"
#     And I click on "disimpan" on "settingsScreen"
#     Then I can see "bookMark" is "displaying" on "settingsScreen"

# Scenario: As a user I should be able to click on share button for a particular Video or Article feed in Grid Mode so that it will be displayed in Disimpan Screen
    
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I click on "list" on "settingsScreen"
#     And I click on "homeScreenTab" on "settingsScreen"
#     And I choose "English" from "homeScreen"
#     And I can see "shareLink" is "displaying" on "homeScreen"
#     And I click on "shareLink" on "homeScreen"
#     And I can see "gmail" is "displayingElem" on "infoSalatScreen"
#     And I can see "drive" is "displayingElem" on "infoSalatScreen"
#     And I can see "messages" is "displayingElem" on "infoSalatScreen"
#     Then I can see "copy" is "displayingElem" on "infoSalatScreen"
        
# Scenario Outline: As a User I should be able to click on Share Button in InfoSolat and then it should display Native Apps
#     And I click on "Yes" on "homeScreen"
#     And I am navigated to "infoSalatScreen"
#     And I click on "solatShareButton" on "infoSalatScreen"
#     And I can see "shareLink" is "displaying" on "infoSalatScreen"
#     And I can see "gmail" is "displayingElem" on "infoSalatScreen"
#     And I can see "drive" is "displayingElem" on "infoSalatScreen"
#     And I can see "messages" is "displayingElem" on "infoSalatScreen"
#     And I can see "copy" is "displayingElem" on "infoSalatScreen"
#     # And I see the "Gmail" and "Messages" and "SavetoDrive" is displayed on "infoSalatScreen"
#     #  | nativeApps      |
#     #  |Messages         | 
#     #  |Gmail            | 
#     #  |Copy to clipboard|
#     #  |Save to Drive    | 
     
#     When I click on "<Apps>" on "infoSalatScreen"
#     Then I can see "<appTextElem>:<appText>" is "displayingText" on "infoSalatScreen"
#     Examples:
#         | Apps          | appText                                                     | appTextElem |
#         | drive         |  My Drive                                                   | myDrive     |     
#         | gmail         |  Astro AWANI https://appgallery.huawei.com/#/app/C101185541 | gmailText   |

# Scenario: As a User I should be able to click on solat Compass Button then I am navigated to Kiblat Screen and need to check the Screen data
#     And I click on "Yes" on "homeScreen"
#     And I am navigated to "infoSalatScreen"
#     And I can see "solatCompassButton" is "displaying" on "infoSalatScreen"
#     When I click on "solatCompassButton" on "infoSalatScreen"
#     And I am navigated to "solatCompassScreen"
#     And I can see "kiblatTitle" is "displaying" on "solatCompassScreen"
#     And I can see "kiblatDescription" is "displayingElem" on "solatCompassScreen"
#     And I can see "kiblatArrowIcon" is "displayingElem" on "solatCompassScreen"
#     And I can see "kiblatBackButton" is "displayingElem" on "solatCompassScreen"
#     And I click on "kiblatBackButton" on "solatCompassScreen"
#     Then I am navigated to "infoSalatScreen"
   
# Scenario: As a user I should be able to call the Country API and Fetch the List of Solat Countries
#     And I click on "Yes" on "homeScreen"
#     And I am navigated to "infoSalatScreen"    
#     And I click on "solatSelectedDropdown" on "infoSalatScreen"
#     When I "GET" the request "https://de-api.eco.astro.com.my/solat/api/v1/country"
#     Then I see "States" which is "rendering" on "infoSalatScreen" is Same as WebService JSON "$.states[*].name"

# Scenario: As a user I should be check the Zones Specific to State API 
#      And I click on "Yes" on "homeScreen"
#      And I am navigated to "infoSalatScreen"    
#      When I "GET" the request "https://de-api.eco.astro.com.my/solat/api/v1/country?zon=all"
#      Then I see "Zones" which is "renderingZonesBasedOnState" on "infoSalatScreen" is Same as WebService JSON "zone"
    
# Scenario Outline: As a user I should be able to See Solat Player Time and Solat Prayer hour in Hari ini and ESOK
#      And I click on "Yes" on "homeScreen"
#      And I am navigated to "infoSalatScreen"
#      And I click on "<prayerTimeTabs>" on "infoSalatScreen"
#      When I see the "prayerTimes" and "hourFormat" and "bellIcon" is displayed on "infoSalatScreen" 

#         |prayerTimes|hourFormat             | bellIcon |
#         |Imsak      | hourRegularExpression | isEnabled|
#         |Subuh      | hourRegularExpression | isEnabled|
#         |Syuruk     | hourRegularExpression | isEnabled|
#         |Zohor      | hourRegularExpression | isEnabled|
#         |Asar       | hourRegularExpression | isEnabled|
#         |Maghrib    | hourRegularExpression | isEnabled|
#         |Isyak      | hourRegularExpression | isEnabled|

#      Examples:
#          | prayerTimeTabs | 
#          | inhari         |
#          | esok           |

# Scenario Outline: As a user I should be able to See the following Elements are displayed on InfoSalat Screen
#      And I click on "Yes" on "homeScreen"
#      And I am navigated to "infoSalatScreen"
#      And I can see "solatPrayerTitle" is "displayingElem" on "infoSalatScreen"
#      And I can see "solatCountDown" is "displayingElem" on "infoSalatScreen"
#      And I can see "solatShareButton" is "displayingElem" on "infoSalatScreen"
#      And I can see "solatCompassButton" is "displayingElem" on "infoSalatScreen"
#      And I can see "solatLocationPrompt" is "displayingElem" on "infoSalatScreen"
#      And I can see "solatLocationText" is "displayingElem" on "infoSalatScreen"
#      And I can see "slolatBackButton" is "displayingElem" on "infoSalatScreen"
#      Then I can see "solatLocationSymbol" is "displayingElem" on "infoSalatScreen"


# Scenario: As a user I should be able to See Solat player title and time should be displaying on Prayer time and hour
#      And I click on "Yes" on "homeScreen"
#      And I am navigated to "infoSalatScreen"
#      And I can see "solatPrayerTitle" is "displaying" on "infoSalatScreen"
#      And I can see "prayerTimes" is "displayingsolatPrayerTitle" on "infoSalatScreen"
#      Then I can see "hourFormat" is "displayingsolatPrayerTitle" on "infoSalatScreen"

# Scenario Outline: As a user I should be able to See the Awaniheader Image,SearchBar and InfosalatIcon on all the tabs except Settings
#     And I click on "Yes" on "homeScreen"
#     And I am navigated to "infoSalatScreen"
#     And I click on "slolatBackButton" on "infoSalatScreen"
#     And I am navigated to "homeScreen"
#     And I click on "<tabName>" on "<screenName>"
#     Then I can see "<IconorImage>" is "displayingElem" on "homeScreen"

#     Examples:
#         |tabName         |screenName    |IconorImage              |
#         | homeScreenTab  |homeScreen    | syokHeaderImage         |
#         | liveVideoTab   |homeScreen    | VideoHeaderImage        |
#         | podCastTab     |homeScreen    | podcastHeaderImage      |
#         | NotificationTab|homeScreen    | NotificationsHeaderImage|
#         |settings        |homeScreen    |settingHeaderTitle       |
#         | homeScreenTab  |homeScreen    | searchButton            |
#         | liveVideoTab   |homeScreen    | searchButton            |
#         | podCastTab     |homeScreen    | searchButton            |
#         | homeScreenTab  |homeScreen    | InfosalatIcon           |
#         | liveVideoTab   |homeScreen    | InfosalatIcon           |
#         | podCastTab     |homeScreen    | InfosalatIcon           |

# Scenario: As a user I should be able to Navigate to InfoSolat Screen once I click on Solat Toggle Button in Settings Screen
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     When I click on "settingSolatToggle" on "settingsScreen"
#     Then I am navigated to "infoSalatScreen"

# Scenario: As a user I should be able to Navigate to Notifications Screen and I should see a List of Notifications

#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I click on "NotificationTab" on "homeScreen"
#     When I am navigated to "notificationsScreen"
#     And I can see "listofNotifications" is "displaying" on "notificationsScreen"
#     And I can see "listofNotificationImages" is "displayingElem" on "notificationsScreen"
#     And I can see "notificationsTime" is "displayingDate" on "notificationsScreen"
#     And I Swipe "UP" on "notificationsScreen"
#     And I can see "listofNotificationImages" is "displayingElem" on "notificationsScreen"
#     And I can see "listofNotifications" is "displaying" on "notificationsScreen"
#     Then I can see "notificationsTime" is "displayingDate" on "notificationsScreen"

# Scenario: As a user I should be able to Navigate to Notifications Screen and I should click on a feed then I should be able to navigate to WebView

#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I click on "NotificationTab" on "homeScreen"
#     When I am navigated to "notificationsScreen"
#     Then I can see "listofNotifications" is "displaying" on "notificationsScreen"
# #     And I click on "NotificationsTitle" on "notificationsScreen"
# #     And I am navigated to "webView"
# #     Then I can see "NotificationsTitle" is "displayed" on "webViewScreen"



# Scenario: As a user I should be able to see the dot Symbol disappeared the top of Notification Icon once I click on NotificationTab

#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I can see "NotificationDot" is "displayingElem" on "settingsScreen"
#     When I click on "NotificationTab" on "settingsScreen"
#     Then I can see "NotificationDot" is "notdisplaying" on "settingsScreen"


# Scenario: As a user I should be able to Navigate to PodCastScreen and should see different podcasts

#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I click on "podCastTab" on "homeScreen"
#     And I am navigated to "podCastScreen"
#     And I can see "Saluran" is "displayingText" on "podCastScreen"
#     And I can see "podCastDisplayImages" is "displayingElem" on "podCastScreen"
#     And I can see "podCastText" is "displaying" on "podCastScreen"
#     And I can see "Terkini" is "displayingText" on "podCastScreen"
#     And I can see "latestPodCastTitle" is "displaying" on "podCastScreen"
#     And I can see "latestPodCastPlay" is "displaying" on "podCastScreen"
#     And I can see "latestPodCastSubTitle" is "displayingDate" on "podCastScreen"
#     And I Swipe "UP" on "podCastScreen"
#     And I can see "latestPodCastTitle" is "displaying" on "podCastScreen"
#     And I can see "latestPodCastPlay" is "displaying" on "podCastScreen"
#     And I can see "latestPodCastSubTitle" is "displayingDate" on "podCastScreen"


# Scenario: As a user I should be able to Navigate to PodCastScreen and Play podCasts mini Player
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I click on "podCastTab" on "homeScreen"
#     And I am navigated to "podCastScreen"
#     And I click on "latestPodCastPlay" on "podCastScreen"
#     And I can see "miniPlayerTitle" is "displaying" on "podCastScreen"
#     And I can see "miniPlayerSubTitle" is "displaying" on "podCastScreen"
#     And I can see "miniPlayerImage" is "displayingElem" on "podCastScreen"
#     When I click on "miniPlayerPause" on "podCastScreen"
#     And I can see "miniPlayerPlay" is "displayingElem" on "podCastScreen"
#     And I click on "miniPlayerPlay" on "podCastScreen"
#     And I can see "miniPlayerPause" is "displayingElem" on "podCastScreen"
#     And I click on "miniPlayerStop" on "podCastScreen"
#     Then I can see "miniPlayerTitle" is "notDisplaying" on "podCastScreen"

# Scenario: As a user I should be able to Navigate to PodCastScreen and Play podCasts Full Player
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I click on "podCastTab" on "homeScreen"
#     And I am navigated to "podCastScreen"
#     And I click on "latestPodCastPlay" on "podCastScreen"
#     And I can see "miniPlayerTitle" is "displaying" on "podCastScreen"
#     And I click on "miniPlayerTitle" on "podCastScreen"
#     And I can see "Podcast" is "displayingText" on "podCastScreen"
#     And I can see "fullPlayerListIcon" is "displayingElem" on "podCastScreen"
#     And I can see "fullPlayerClose" is "displayingElem" on "podCastScreen"
#     And I can see "fullPlayerImage" is "displayingElem" on "podCastScreen"
#     And I can see "fullPlayerMediaTitle" is "displaying" on "podCastScreen"
#     And I can see "fullPlayerMediaSubTitle" is "displaying" on "podCastScreen"
#     And I click on "fullPlayerPause" on "podCastScreen"
#     And I can see "fullPlayerPlay" is "displayingElem" on "podCastScreen"
#     When I click on "fullPlayerPlay" on "podCastScreen"
#     And I can see "fullPlayerPause" is "displayingElem" on "podCastScreen"
#     And I can see "fullPlayerRewind" is "displayingElem" on "podCastScreen"
#     And I can see "fullPlayerForward" is "displayingElem" on "podCastScreen"
#     And I can see "fullPlayerPrevious" is "displayingElem" on "podCastScreen"
#     And I can see "fullPlayerNext" is "displayingElem" on "podCastScreen"
#     And I can see "fullPlayerDurationSlider" is "displayingElem" on "podCastScreen"
#     And I can see "fullPlayerCurrentTime" is "displayingElem" on "podCastScreen"
#     And I click on "fullPlayerClose" on "podCastScreen"
#     Then I can see "miniPlayerTitle" is "displaying" on "podCastScreen"

# Scenario: As a User I should be able to see the Full Player List in Full Player
    
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I click on "podCastTab" on "homeScreen"
#     And I am navigated to "podCastScreen"
#     And I click on "latestPodCastPlay" on "podCastScreen"
#     And I can see "miniPlayerTitle" is "displaying" on "podCastScreen"
#     And I click on "miniPlayerTitle" on "podCastScreen"
#     And I can see "Podcast" is "displayingText" on "podCastScreen"
#     And I can see "fullPlayerListIcon" is "displayingElem" on "podCastScreen"
#     When I click on "fullPlayerListIcon" on "podCastScreen"
#     And I can see "podCastPlayList" is "displaying" on "podCastScreen" 
#     Then I can see "latestPodCastPlay" is "displayingElem" on "podCastScreen"


# Scenario: As a user I should be able to click on Hero Banner and should be able to navigate to WebView

#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I click on "homeScreenTab" on "settingsScreen"
#     When I am navigated to "homeScreen"
#     And I choose "English" from "homeScreen"
#     And I click on "feedTitle" on "homeScreen"
#     And I am navigated to "webView"
#     Then I can see "feedTitle" is "displayed" on "webViewScreen"

    
# Scenario: As a user I should be able to see Multiple Options in the Settings Screen 
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I can see "loginButton" is "displayingElem" on "settingsScreen" 
#     And I can see "settingsTitle" is "displaying" on "settingsScreen"
#     When I can see "videoAutoPlay" is "displaying" on "settingsScreen" 
#     And I can see "infoSolatSettingTitle" is "displaying" on "settingsScreen"
#     And I can see "notificationsTitle" is "displayingElem" on "settingsScreen" 
#     And I can see "beritaDisplayTitle" is "displayingElem" on "settingsScreen"
#     And I can see "infoAwaniTitle" is "displayingElem" on "settingsScreen" 
#     And I can see "aboutUs" is "displaying" on "settingsScreen"
#     Then I can see "contactUs" is "displaying" on "settingsScreen" 

# Scenario: As a user I should be able to see Contact Us in the Settings Screen and when we click on it, it should navigate to Gmail
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I can see "contactUs" is "displaying" on "settingsScreen"
#     When I click on "contactUs" on "settingsScreen"
#     Then I can see "googleEmailSubject" is "displayingElem" on "settingsScreen"

# Scenario: As a user I should be able to see About Us in the Settings Screen and when we click on it, it should display List of Options
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I can see "aboutUs" is "displaying" on "settingsScreen"
#     When I click on "aboutUs" on "settingsScreen"
#     And I can see "generalTerms" is "displayingElem" on "settingsScreen"
#     And I can see "privacyPolicy" is "displayingElem" on "settingsScreen"
#     And I can see "applicationVersion" is "displayingElem" on "settingsScreen"
#     Then I can see "version" is "displayingElem" on "settingsScreen"

# Scenario Outline: As a user I should be able to login using valid Credentials
#     And I click on "No" on "homeScreen"
#     And I am navigated to "settingsScreen"
#     And I can see "loginButton" is "displayingElem" on "settingsScreen" 
#     And I click on "loginButton" on "settingsScreen"
#     And I am navigated to "loginWebView"
#     When I attempt to "<user_action>" with "<creds_type>" credentials
#     Then I see "<user_action>" is "<result>"
#     Examples:
#     | user_action | creds_type     | result      |
#     | login       | valid email    | successful  |
#     | login       | valid mobile   | successful  |
#     | login       | invalid email  | failure     |
#     | login       | invalid mobile | failure     |
#     | login       | invalid creds  | failure     |